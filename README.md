Solgrind
====================

# Summary
The Solgrind project has many different modules for spatiotemporal frequent pattern mining from evolving region trajectories.

It includes a number of useful classes and methods.

* Base data types (time-geometry pair, trajectory, event instance, event type, event co-occurrence and event sequences). These can be found under '$/base/' folder.
* Pattern mining algorithms (STCOP and STESs). These can be found under '$/algo/' folder.
* Measures for spatiotemporal co-occurrences. These can be found under '$/algo/' folder.
* Solar graph index (solgrind) for graph-based index. The source code for graph index is under '$/algo/' folder. 
* File I/O for reading data from files and writing (under '$/fileio/' folder.)
* Database connections (from early codebases)
* Interpolation algorithms for evolving region trajectories. (under '$/interpolation/' folder.)


- - -
# Contributors
Berkay Aydin, Ahmet Kucuk, Soukaina Filali Boubrahimi, Dustin Kempton



