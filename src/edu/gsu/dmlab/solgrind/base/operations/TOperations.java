package edu.gsu.dmlab.solgrind.base.operations;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

import edu.gsu.dmlab.solgrind.base.types.essential.TInterval;
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;

/**
 * Implementation of some temporal operations for Trajectory objects
 * @author berkay - Aug 1, 2016
 *
 */

public class TOperations {

	/**
	 * Method for temporal intersection between two trajectory. 
	 * Returns the set of time interval objects where there exists
	 * a temporal co-existence relation between two trajectories
	 * given as parameters 
	 * @param traj1
	 * @param traj2
	 * @return SortedSet of TInterval objects
	 */
	public static TreeSet<TInterval> tIntersection( Trajectory traj1, Trajectory traj2 ){ //TODO test it
		TreeSet<TInterval> intersectionIntervals = new TreeSet<>();
		TreeSet<TInterval> t1Intervals = traj1.getTimeIntervals();
		TreeSet<TInterval> t2Intervals = traj2.getTimeIntervals();
		
		
		Iterator<TInterval> iter1 = t1Intervals.iterator();
		Iterator<TInterval> iter2 = t2Intervals.iterator();
		TInterval cIntv1 = iter1.next();
		TInterval cIntv2 = iter2.next();
		
		while(iter1.hasNext()){
			
			if(cIntv1.endsBeforeStartOf(cIntv2)){
				cIntv1 = iter1.next();
			} else if( cIntv2.endsBeforeStartOf(cIntv1) ){
				cIntv2 = iter2.next();
			} else { // they must be overlapping
				TInterval intersectionInterval = cIntv1.intersection(cIntv2);
				intersectionIntervals.add(intersectionInterval);
				if(cIntv1.endsBefore(cIntv2)){
					cIntv1 = iter1.next();
				} else if(cIntv2.endsBefore(cIntv1)){
					cIntv2 = iter2.next();
				} else{ // they end at the same time
					cIntv1 = iter1.next();
					cIntv2 = iter2.next();
				}
			}
		}
		return intersectionIntervals;
	}
	
	public static boolean tIntersects( Trajectory traj1, Trajectory traj2 ){ //TODO test it
		TInterval t1ti = new TInterval(traj1.getStartTime(), traj1.getEndTime());
		TInterval t2ti = new TInterval(traj2.getStartTime(), traj2.getEndTime());
		
		return t1ti.overlaps(t2ti);
		
	}

	public static TreeSet<TInterval> merge(TreeSet<TInterval> xcoTimeIntervals, TreeSet<TInterval> xcoTimeIntervals2) {
		
		ArrayList<TInterval> tiset1 = new ArrayList<>(xcoTimeIntervals);
		ArrayList<TInterval> tiset2 = new ArrayList<>(xcoTimeIntervals2);
		//TODO finish here
		
		
		
		return null;
	}
	
	
}
