package edu.gsu.dmlab.solgrind.base.operations;

import com.vividsolutions.jts.geom.Envelope;

import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;

public class SOperations {

	public static boolean sIntersectsMBR(Trajectory traj1, Trajectory traj2) {
		
		Envelope t1mbr = traj1.getMBR();
		Envelope t2mbr = traj2.getMBR();
		
		return t1mbr.intersects(t2mbr);
	}

}
