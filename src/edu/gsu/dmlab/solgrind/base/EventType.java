package edu.gsu.dmlab.solgrind.base;

/**
 * @author berkay - Jul 12, 2016
 *
 */
public class EventType implements Comparable<EventType>{

	private String type;
	
	public EventType(String EventType) {
        type = EventType.toLowerCase();
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object t) {

        if(this == t) return true;

        if(!(t instanceof EventType)) {
            return false;
        }
        return this.type.equalsIgnoreCase(((EventType)t).getType());
    }

    public String getColor() {
        switch (type) {
            case "ar":
                return "red";
            case "ch":
                return "black";
            case "ef":
                return "green";
            case "fi":
                return "blue";
            case "sg":
                return "purple";
            case "ss":
                return "yellow";
            case "f1":
                return "red";
            case "f2":
                return "black";
            case "f3":
                return "green";
            case "f4":
                return "blue";
            case "f5":
                return "purple";
            case "f6":
                return "yellow";
        }
        return "grey";
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    
    public boolean equalsIgnoreCase(EventType t) {
        return this.type.equalsIgnoreCase(t.type);
    }
    
	public String toString(){
		return type;
	}

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new EventType(type);
    }

    @Override
    public int compareTo(EventType o) {
        return type.compareToIgnoreCase(o.getType());
    }
}
