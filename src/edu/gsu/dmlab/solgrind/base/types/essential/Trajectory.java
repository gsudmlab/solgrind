package edu.gsu.dmlab.solgrind.base.types.essential;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;

import edu.gsu.dmlab.solgrind.base.operations.STOperations;

import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author berkay - Jul 12, 2016
 *
 */
public class Trajectory {
	
	private SortedSet<TGPair> tgpairs; //time geometry pairs of trajectory
	
	public Trajectory (){
		tgpairs = new TreeSet<>();
	}
	
	public Trajectory( Collection<TGPair> timeGeometryPairs ) {
		
		tgpairs = new TreeSet<>();
		tgpairs.addAll(timeGeometryPairs);
		
	}
	
	public boolean isEmpty(){
		return tgpairs == null || tgpairs.isEmpty();
	}

	public boolean isValid(){
		if(isEmpty()){
			return true;
		}
		for (TGPair tgp : tgpairs){
			if(!tgp.isValid()){
				return false;
			}
		}
		return true;
	}
	
	public String toString(){
		String str = "{\n";
		for(TGPair tgp : tgpairs){
			str += tgp.toString() + "\n";
		}
		str += "}";
		return str;
	}
	
	public Envelope getMBR(){
		Envelope env = new Envelope();
		for(TGPair tgp : tgpairs){
			env.expandToInclude(tgp.getEnvelope());
		}
		return env;
	}
	
	public Trajectory getSegment(long start, long end){
		Trajectory segment = new Trajectory();
		TInterval desiredTInterval=new TInterval(start, end);
		
		Iterator<TGPair> pairs = tgpairs.iterator();
		while(pairs.hasNext()){
			TGPair tgp = pairs.next();
			//get intersection between an individual time interval and desired interval
			TInterval intersectionInterval = tgp.getTInterval().intersection( desiredTInterval ); 
			if(intersectionInterval != null){
				segment.addTGPair( intersectionInterval.getStartTime(), intersectionInterval.getEndTime(), tgp.getGeometry() );
			}
		}
		return segment;
	}

	public void addTGPair(long startTime, long endTime, Geometry geometry) {
		TGPair tgp = new TGPair(startTime, endTime, geometry);
		this.tgpairs.add(tgp);
	}

	public void addTGPair(TGPair tgp) {
		this.tgpairs.add(tgp);
	}

	public long getStartTime() {
		return tgpairs.first().getTInterval().getStartTime();
	}
	
	public long getEndTime() {
		return tgpairs.last().getTInterval().getEndTime();
	}
	
	public TreeSet<TInterval> getTimeIntervals(){
		TreeSet<TInterval> intervalSet = new TreeSet<>();
		for(TGPair tgp : tgpairs){
			
			intervalSet.add(tgp.getTInterval());
		}
		return intervalSet;
	}

	public TreeSet<TGPair> getTGPairs(){
		return (TreeSet<TGPair>) tgpairs;
	}
	
	/**
	 * Retuns the number of TGPairs in the trajectory
	 * @return
	 */
	public int getTGPairSize(){
		return tgpairs.size();
	}

	/**
	 * Based on a sampling interval, sample the time-geometry pairs in the trajectory
	 * @param tailBuffer
	 * @param samplingInterval
	 * @return
	 */
	public Trajectory sampleTGPairs(Trajectory traj, long samplingInterval) {
		
		if(samplingInterval == -1L){
			return traj;
		}
		
		Trajectory sampledTraj = new Trajectory();
		
		TGPair[] tgpArray = (TGPair[]) traj.tgpairs.toArray();
		for(int i = 0; i < traj.tgpairs.size(); i++){
			TGPair tgp = tgpArray[i];

			long tgpStartTime = tgp.getTInterval().getStartTime();
			long tgpEndTime = tgp.getTInterval().getEndTime();
			long lifespan = tgpEndTime - tgpStartTime;
			
			long sampledTgpStart = tgpStartTime;
			long sampledTgpEnd = tgpStartTime + samplingInterval;
			
			while(sampledTgpEnd <= tgpEndTime){
				//	start from first time interval start and keep iterating with sampling interval	
				Geometry sampledGeom =null;
				double samplingRatio = (double) sampledTgpStart / (double) lifespan;
				
				if(i != tgpArray.length){
					sampledGeom = STOperations.interpolate( tgp.getGeometry(), tgpArray[i+1].getGeometry(), samplingRatio );
				} else{
					sampledGeom = STOperations.interpolate( tgp.getGeometry() , sampledTgpStart-tgpStartTime );
				}
				
				sampledTraj.addTGPair(sampledTgpStart, sampledTgpEnd, sampledGeom);				
				
				sampledTgpStart += samplingInterval;
				sampledTgpEnd += samplingInterval;
				
				if(sampledTgpStart < tgpEndTime && sampledTgpEnd > tgpEndTime){
					sampledTgpEnd = tgpEndTime;
				}
			}
		}
		
		return sampledTraj;
	}

	public double getVolume() {
		
		double volume = 0.0;
		for( TGPair tgp : tgpairs ){
			volume += tgp.getVolume();
		}
		
		return volume;
	}
	
	
}
