/**
 * 
 */
package edu.gsu.dmlab.solgrind.base.types.essential;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;

/**
 * @author berkay - Jul 12, 2016
 *
 */
public class TGPair implements Comparable<TGPair>{

	
	
	private TInterval interval;
	private Geometry geometry;
	
	
	public TGPair(long start, long end, Geometry geom){
		interval = new TInterval(start, end);
		geometry = new GeometryFactory().createGeometry(geom);
	}
	
	public TGPair(long stime, Geometry geom){
		try {
			interval = new TInterval(stime);
		} catch (SamplingPeriodException e) {
			e.printStackTrace();
		}
		geometry = new GeometryFactory().createGeometry(geom);
	}
	
	public TGPair(String start, String end, Geometry geom){
		interval = new TInterval(start, end);
		geometry = new GeometryFactory().createGeometry(geom);
	}
	
	public TGPair(String startTimestamp, Geometry geom){
		try {
			interval = new TInterval(startTimestamp);
		} catch (SamplingPeriodException e) {
			e.printStackTrace();
		}
		geometry = new GeometryFactory().createGeometry(geom);
	}
	
	public boolean isValid() {
		return false;
	}

	public Envelope getEnvelope() {
		return geometry.getEnvelopeInternal();
	}

	public double getVolume() {
		return this.getTInterval().getLifespan() * this.getGeometry().getArea();
	}
	
	/**
	 * Spatiotemporal overlaps method. Checks if the given time geometry pair
	 * both temporally and spatially intersects with the parameter tgp.
	 * @return True if overlap occurs, False otherwise
	 */
	public boolean stOverlaps(TGPair tgp){
		boolean tOverlaps = this.tOverlaps(tgp);
		boolean sOverlaps = this.sOverlaps(tgp);
		return tOverlaps && sOverlaps;
	}

	/**
	 * Spatial-only overlaps method.
	 * Checks if the geometries of the objects overlap in space
	 * @param tgp
	 * @return
	 */
	public boolean sOverlaps(TGPair tgp) {
		return this.geometry.overlaps(tgp.geometry);
	}
	/**
	 * Temporal-only overlaps method
	 * Checks of the intervals of the objects overlap in time
	 * @param tgp
	 * @return
	 */
	public boolean tOverlaps(TGPair tgp) {
		return this.interval.overlaps(tgp.interval);
	}
	
	public boolean tStartsAfter(TGPair tgp){
		return this.interval.startsAfter(tgp.interval);
	}

	public TInterval getTInterval() {
		return this.interval;
	}

	public Geometry getGeometry() {
		return geometry;
	}
	
	public void setGeometry(Geometry geom){
		geometry = new GeometryFactory().createGeometry(geom);
	}

	@Override
	public int compareTo(TGPair tgp) {
		if(this.getTInterval().getStartTime() < tgp.getTInterval().getStartTime()){
			return -1;
		}else if(this.getTInterval().getStartTime() > tgp.getTInterval().getStartTime()){
			return 1;
		}else /* if(tgp1.getTInterval().getStartTime() == tgp.getTInterval().getStartTime()) */{
			return 0;
		}
	}

	@Override
	public String toString() {
		return "TGPair{" +
				"interval=" + interval +
				", geometry=" + geometry +
				'}';
	}
}
//
//class TGPairComparator implements Comparator<TGPair>{
//
//	@Override
//	public int compare(TGPair tgp1, TGPair tgp2) {
//		if(tgp1.getTInterval().getStartTime() < tgp2.getTInterval().getStartTime()){
//			return -1;
//		}else if(tgp1.getTInterval().getStartTime() > tgp2.getTInterval().getStartTime()){
//			return 1;
//		}else /* if(tgp1.getTInterval().getStartTime() == tgp2.getTInterval().getStartTime()) */{
//			return 0;
//		}
//	}
//
//}
