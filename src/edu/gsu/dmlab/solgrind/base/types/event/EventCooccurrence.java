/**
 * 
 */
package edu.gsu.dmlab.solgrind.base.types.event;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import edu.gsu.dmlab.solgrind.base.EventType;

/**
 * This class is will be used for ST co-occurrence pattern.
 * (Naming is EventCooccurrence because not every set of 
 * event co-occurrences are necessarily frequent patterns)
 * @author berkay - Jul 12, 2016
 */
public class EventCooccurrence implements Comparable<EventCooccurrence>{
	
	private Set<EventType> eventTypes;
	private double prevalence = 0.0;
	
	public EventCooccurrence( Collection<EventType> eventTypes ){
		this.eventTypes = new TreeSet<EventType>(eventTypes);
		prevalence = 0.0;
	}
	
	public EventCooccurrence(){
		eventTypes = new TreeSet<>();
		prevalence = 0.0;
	}
	
	/** copy constructor
	 * @param eventCooccurrence
	 */
	public EventCooccurrence(EventCooccurrence eventCooccurrence) {
		eventTypes = new TreeSet<>();
		for(EventType otherEvent : eventCooccurrence.getEventTypes()){
			eventTypes.add(new EventType(otherEvent.getType()));
		}
		this.prevalence = eventCooccurrence.getPrevalence();
	}

	public void addEventType(EventType eventType) {
		this.eventTypes.add(eventType);
	}
	
	public Set<EventType> getEventTypes(){
		return eventTypes;
	}
	
	/**
	 * This function returns the size-(k-1) subsets of a size-k event cooccurrence
	 * object. For any size-k event cooccurrence it returns a size-k array list
	 * @return a size-k arraylist of size-(k-1) subsets of an event cooccurrence
	 */
	public ArrayList<EventCooccurrence> getImmediateSubsets(){
		ArrayList<EventCooccurrence> subsets = new ArrayList<>();
		
		ArrayList<EventType> eventTypes = new ArrayList<>(this.getEventTypes());
		
		for(int i = 0; i < eventTypes.size(); i++){
			ArrayList<EventType> temp = new ArrayList<>();
			for(int j = 0; j < eventTypes.size(); j++){
				if(j != i){
					temp.add(eventTypes.get(j));
				}
			}
			EventCooccurrence subset = new EventCooccurrence(temp);
			subsets.add(subset);
		}
		return subsets;
	}
	
	public String toString(){
		return "EventCO:"+this.eventTypes.toString();
	}
	
	public int getCardinality(){
		return this.eventTypes.size();
	}

	public boolean equals(Object et){
		if(et instanceof EventCooccurrence){
			if(this != null && et != null){
				return this.eventTypes.containsAll( ((EventCooccurrence)et).getEventTypes());
			} else{ //at least one of the ECs is null
				return false;
			}
		} else{
			return false;
		}
	}
	
	public int hashCode() {
		int i = 13;
		if( this != null ){
			for(EventType e : this.eventTypes){
				i += e.hashCode();
			}
		}
		return i;
	}

	public EventCooccurrence union(EventCooccurrence eventCooccurrence) {
		SortedSet<EventType> es1 = new TreeSet<EventType>(eventTypes);
		SortedSet<EventType> es2 = new TreeSet<EventType>(eventCooccurrence.getEventTypes());
		es1.addAll(es2);
		return new EventCooccurrence(es1);
	}

	@Override
	public int compareTo(EventCooccurrence o) {
		if(this.getCardinality() > o.getCardinality()){
			return -1;
		} else if(this.getCardinality() < o.getCardinality()){
			return 1;
		} else{
			return this.eventTypes.toString().compareTo(o.eventTypes.toString());
		}
	}

	public void setPrevalence(double pi) {
		this.prevalence = pi;
	}
	
	public double getPrevalence(){
		return prevalence;
	}

}
