package edu.gsu.dmlab.solgrind.base.types.instance;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.gsu.dmlab.solgrind.base.Instance;

/**
 * 
 * @author berkay - Aug 1, 2016
 *
 */

public class InstanceSimilarity {

	private Set<InstanceData> similarityData;
	private double similarityIndex;
	private String similarityIndexType;
	
	public InstanceSimilarity(){
		similarityData = new HashSet<InstanceData>();
	}
	
	public InstanceSimilarity(InstanceData data){
		similarityData = new HashSet<InstanceData>();
		similarityData.add(data);
	}
	
	public InstanceSimilarity(List<InstanceData> data){
		similarityData = new HashSet<InstanceData>(data);
	}
	
	public static InstanceSimilarity createInstanceSequence( Collection<Instance> instances ){
		InstanceSimilarity similarity = new InstanceSimilarity();
		for( Instance inst : instances ){
			similarity.append( InstanceData.createInstanceData(inst) );
		}
		return similarity;
	}

	private void append(InstanceData instanceData) {
		similarityData.add(instanceData);
	}
	
	public void setSimilarityIndex(double si){
		similarityIndex = si;
	}
	
	public double getSimilarityIndex(){
		return similarityIndex;
	}
	
	public void setSimilarityIndexType(String siType){
		if(siType.equals("Edit") || siType.equals("Euclidean") || siType.equals("DTW") ){
			similarityIndexType = siType;
		} else{
			similarityIndexType = "N/A";
		}
	}
	
	public String getSimilarityIndexType(){
		return similarityIndexType;
	}
	
}
