package edu.gsu.dmlab.solgrind.base.types.instance;

import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.operations.TOperations;
import edu.gsu.dmlab.solgrind.base.types.essential.TInterval;
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;
import edu.gsu.dmlab.solgrind.base.types.event.EventCooccurrence;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


/**
 * This class implements utility operations and necessary operations for 
 * pattern instances in STCOP mining
 * @author berkay
 *
 */
public class InstanceCooccurrence {
	public static final double INVALID_CCE = Double.MIN_VALUE;
	private Set<InstanceData> cooccurrenceData;
	private double cce;					/* Co-occurrence coefficient */
	private String cceType;
	
	private Trajectory iTraj = null;
	private Trajectory uTraj = null;
	private TreeSet<TInterval> xcoTI = null;
	
	public InstanceCooccurrence(){
		cooccurrenceData = new HashSet<InstanceData>();
		cce = INVALID_CCE;
	}
	
	public InstanceCooccurrence(InstanceData data){
		cooccurrenceData = new HashSet<InstanceData>();
		cooccurrenceData.add(data);
	}
	
	public InstanceCooccurrence(List<InstanceData> data){
		cooccurrenceData = new HashSet<InstanceData>(data);
	}
	
	public static InstanceCooccurrence createInstanceCooccurrence( Collection<Instance> instances ){
		
		InstanceCooccurrence coOccurrence = new InstanceCooccurrence();
		for( Instance inst : instances ){
			coOccurrence.append( InstanceData.createInstanceData(inst) );
		}
		return coOccurrence;
	}

	public void append(InstanceData instanceData) {
		cooccurrenceData.add(instanceData);
	}
	
	public Set<InstanceData> getInstanceData(){
		return cooccurrenceData;
	}
	
	public EventCooccurrence getEventCooccurrenceType(){
		EventCooccurrence ec = new EventCooccurrence();
		for (InstanceData idata : cooccurrenceData){
			ec.addEventType(idata.type);
		}
		return ec;
	}
	
	public String getInstanceIdOfType(EventType et){
		for(InstanceData idata : cooccurrenceData){
			if(idata.type.equals(et)){
				return idata.id;
			}
		}
		return null;
	}

	public double getCce() {
		return cce;
	}

	public void setCce(double cce) {
		this.cce = cce;
	}

	public String getcceType() {
		return cceType;
	}

	public void setcceType(String cceType) {
		if(cceType.equals("J") || cceType.equals("J*") || cceType.equals("OMAX") ){
			this.cceType = cceType;
		} else{
			cceType = "N/A";
		}
	}

	public Trajectory getIntersectionTrajectory() {
		return iTraj;
	}

	public void setIntersectionTrajectory(Trajectory iTraj) {
		this.iTraj = iTraj;
	}

	public Trajectory getUnionTrajectory() {
		return uTraj;
	}

	public void setUnionTrajectory(Trajectory uTraj) {
		this.uTraj = uTraj;
	}

	public TreeSet<TInterval> getXcoTimeIntervals() {
		return xcoTI;
	}

	public void setXcoTimeIntervals(TreeSet<TInterval> xcoTI) {
		this.xcoTI = xcoTI;
	}
	
	public TreeSet<TInterval> mergeXcoTimeIntervals(InstanceCooccurrence ic1,
			InstanceCooccurrence ic2){
		
		return TOperations.merge(ic1.getXcoTimeIntervals(), ic2.getXcoTimeIntervals()); //TODO implement this
	}
	
	public boolean isCceValid(){
		return (cce != InstanceCooccurrence.INVALID_CCE) && (cce <= 1.0) && (cce >= 0.0);
	}
	
	public String toString(){
		return this.cooccurrenceData.toString() + "\tCCE: " + this.getCce(); 
	}
	
}
