package edu.gsu.dmlab.solgrind.base.types.instance;

import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.index.InstanceVertex;

public class InstanceData {
	public final String id;
	public final EventType type;

	public InstanceData(String id, EventType type) {
		this.id = id;
		this.type = type;
	}
	
	public InstanceData(Instance ins){
		this.id = ins.getId();
		this.type = ins.getType();
	}
	
	public static InstanceData createInstanceData(Instance inst){
		return new InstanceData(inst.getId(), inst.getType());
	}
	
	public boolean equals(InstanceData ins){
		return this.id.equalsIgnoreCase(ins.id) && this.type.equals(ins.type);
	}
	
	public int hashCode(){
		return this.type.getType().hashCode() + id.hashCode() ;
	}
	
	public String toString(){
		return type.toString() + ":"+ id;
	}
	
	public static InstanceData createFromInstanceVertex(InstanceVertex iv){
		return new InstanceData(iv.getId(), iv.getType());
	}
	public static InstanceVertex convertToInstanceVertex(InstanceData idata){
		return new InstanceVertex(idata.id, idata.type);
	}
	
	
}