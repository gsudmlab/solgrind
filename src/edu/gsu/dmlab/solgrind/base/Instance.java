package edu.gsu.dmlab.solgrind.base;

import edu.gsu.dmlab.solgrind.base.types.essential.TInterval;
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;

/**
 * Class for modeling spatiotemporal event instances. Instances are moving objects
 * modeled by Trajectory objects
 * @author berkay - Jul 14, 2016
 *
 */

public class Instance implements Comparable<Instance>{

	private static final EventType INVALID_TYPE = new EventType("INVALID_EVENT_TYPE");
	
	private String id;
	private EventType type;
	private Trajectory trajectory;

	public Instance(String iid, EventType e) {
        this.id = iid;
        setType(new EventType(e.getType()));
        trajectory = new Trajectory();
    }
	
	public Instance(String iid){
		setId(iid);
		setType(INVALID_TYPE);
		trajectory = new Trajectory();
	}
	
	public Instance(String iid, EventType e, Trajectory traj){
		setId(iid);
		setType(e);
		trajectory = traj;
	}

	public TInterval getInterval() {
		return new TInterval(getStartTime(), getEndTime());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public EventType getType() {
		return type;
	}

	public void setType(EventType type) {
		this.type = type;
	}
	
	public long getStartTime(){
		return trajectory.getStartTime();
	}
	
	public long getEndTime(){
		return trajectory.getEndTime();
	}
	
	public Trajectory getTrajectory(){
		return this.trajectory;
	}
	
	public boolean equals(Instance ins){
		return this.id == ins.id && this.type.equals(ins.type); 
	}
	
	@Override
	public int hashCode() {
		return this.type.getType().hashCode() + id.hashCode() ;
	}

	@Override
	public String toString() {
		return "Instance{" +
				"id='" + id + '\'' +
				", type=" + type +
				", trajectory=" + trajectory +
				'}';
	}

	@Override
	public int compareTo(Instance o) {
		return new Long(this.getStartTime()).compareTo(o.getStartTime())  ;
	}
}
