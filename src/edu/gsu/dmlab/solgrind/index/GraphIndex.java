package edu.gsu.dmlab.solgrind.index;

import com.vividsolutions.jts.io.ParseException;
import edu.gsu.dmlab.solgrind.SolgrindConstants;
import edu.gsu.dmlab.solgrind.algo.SequenceUtils;
import edu.gsu.dmlab.solgrind.algo.measures.significance.JaccardStar;
import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.database.SolgrindDBConnectionProvider;
import edu.gsu.dmlab.solgrind.database.helper.DotOperations;
import edu.gsu.dmlab.solgrind.database.interfaces.SolgrindDBConnection;

import org.jgrapht.alg.BronKerboschCliqueFinder;
import org.jgrapht.graph.DirectedMultigraph;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.Map.Entry;


public class GraphIndex {

	//	public static HashMap<EventType, Path> EVENT_TYPES;
	public static String[] EVENT_TYPES;
	public final Map<String, Long> eventIdByStartTime = new HashMap<>();
	public final Map<String, List<Instance>> originalMap = new HashMap<>();
	//public final Map<String, List<Instance>> headMap = new HashMap<>();
	//public final Map<String, List<Instance>> tailMap = new HashMap<>();

	DirectedMultigraph<InstanceVertex, RelationEdge> solgrind = new DirectedMultigraph<>(RelationEdge.class);

	SequenceGraph solgrindSequence;
	CooccurrenceGraph solgrindCooccurrence;
	SimilarityGraph solgrindSimilarity;

	public GraphIndex(String[] eventTypes){

		GraphIndex.EVENT_TYPES = eventTypes;
		solgrindCooccurrence = new CooccurrenceGraph(RelationEdge.class);
		solgrindSequence = new SequenceGraph();
		solgrindSimilarity = new SimilarityGraph(RelationEdge.class);


		
	}
	
	public GraphIndex(Map<EventType, Set<Instance> > instanceMap){

		GraphIndex.EVENT_TYPES = new String[instanceMap.size()];
		int i = 0;
		for(Entry<EventType, Set<Instance>> entry : instanceMap.entrySet()){
			GraphIndex.EVENT_TYPES[i]=entry.getKey().toString();
			originalMap.put(entry.getKey().toString(), new ArrayList<>(entry.getValue()));
			i++;
		}
		solgrindCooccurrence = new CooccurrenceGraph(RelationEdge.class);
		solgrindSequence = new SequenceGraph();
		solgrindSimilarity = new SimilarityGraph(RelationEdge.class);

	}

	public void buildFromInstances(Map<EventType, Set<Instance>> instanceMap, Map<String, Long> runTimes) throws IOException, InterruptedException {


		Map<String, List<Instance>> headMap = new HashMap<>();
		Map<String, List<Instance>> tailMap = new HashMap<>();
		long t1 = System.currentTimeMillis();
		for(Map.Entry<EventType, Set<Instance>> entry: instanceMap.entrySet()) {

			String eventType = entry.getKey().toString();

			String headEventName = eventType + "_head";
			String tailEventName = eventType + "_tail";
			Instance instance;
			int counter = 0;
			int totalCount =  entry.getValue().size();
			Iterator<Instance> iterator = entry.getValue().iterator();
			while(iterator.hasNext()) {
				instance = iterator.next();
				headMap.putIfAbsent(headEventName, new ArrayList<>());
				tailMap.putIfAbsent(tailEventName, new ArrayList<>());
				eventIdByStartTime.put(instance.getId(), instance.getStartTime());

				counter++;
				if(counter % 100 == 0) {
					System.out.println("Head and Tail window generation in progress for(" 
									+ eventType.toString() + "): " +  
										+ counter + " of " + totalCount);
				}

				headMap.get(headEventName).add(SequenceUtils.generateHeadwithRatio(instance, SolgrindConstants.H_R));
				tailMap.get(tailEventName).add(SequenceUtils.generateTailWindowWithRatio(instance, SolgrindConstants.T_R, SolgrindConstants.BD, SolgrindConstants.TV));
			}
		}
		System.out.println("Building Sequence Graph");
		long t2 = System.currentTimeMillis();
		runTimes.put("Run time for Head-Tail-Generation (secs)", (t2- t1) /1000);
		t1 = System.currentTimeMillis();
		buildSequenceGraph(headMap, tailMap);
		t2 = System.currentTimeMillis();
		runTimes.put("Run time for Graph-Build-Follow-Relations (secs)", (t2- t1) /1000);
	}

	public void buildFromDatabase(String[] eventTypes) {

		Map<String, List<Instance>> headMap = new HashMap<>();
		Map<String, List<Instance>> tailMap = new HashMap<>();
		GraphIndex.EVENT_TYPES = eventTypes;
		SolgrindDBConnection connection = SolgrindDBConnectionProvider.getSimpleDBConnection();

		for(String s: EVENT_TYPES) {
			for(Instance i: connection.getAllInstances(s, s)) {
				eventIdByStartTime.put(i.getId(), i.getStartTime());
			}
			String headEventName = s + "_head";
			String tailEventName = s + "_tail";

//			originalMap.put(s, SolgrindDBConnectionProvider.getSimpleDBConnection().getAllInstances(s, s));
			headMap.put(headEventName, SolgrindDBConnectionProvider.getSimpleDBConnection().getAllInstances(headEventName, s));
			tailMap.put(tailEventName, SolgrindDBConnectionProvider.getSimpleDBConnection().getAllInstances(tailEventName, s));
		}
//		buildCooccurrenceGraph();
		buildSequenceGraph(headMap, tailMap);
	}

	public void buildSeqFromDOT(String seqDotFile) {

		if(Files.exists(Paths.get(seqDotFile))){
			DotOperations.readDotFile(solgrindSequence, seqDotFile);
		}
	}

	public void buildCoFromDOT(String cooccuranceDotFile) {

		if(Files.exists(Paths.get(cooccuranceDotFile))){
			DotOperations.readDotFile(solgrindCooccurrence, cooccuranceDotFile);
		}
	}

	public void buildFromDOTFile(String cooccuranceDotFile, String seqDotFile) {

		buildSeqFromDOT(seqDotFile);
		buildCoFromDOT(cooccuranceDotFile);
	}

	public void buildSequenceGraph(Map<String, List<Instance>> headMap, Map<String, List<Instance>> tailMap){
		for(int i = 0; i < EVENT_TYPES.length; i++) {
			for(int k = 0; k < EVENT_TYPES.length; k++) {
				findIntersectingHeadAndTails(headMap, tailMap, EVENT_TYPES[i], EVENT_TYPES[k]);
			}
		}
	}

	public void buildCooccurrenceGraph(){

		for(int i = 0; i < EVENT_TYPES.length; i++) {
			for(int k = i+1; k < EVENT_TYPES.length; k++) {
				System.out.println("Building co-occurrence graph for " + EVENT_TYPES[i] + " and " + EVENT_TYPES[k]);
				
				findCooccurences(EVENT_TYPES[i], EVENT_TYPES[k]);
			}
		}
	}

	public void createHeadAndTailRecords() throws ParseException {

		for(String eventName: EVENT_TYPES) {

			SolgrindDBConnection connection = SolgrindDBConnectionProvider.getSimpleDBConnection();
			List<Instance> all = connection.getAllInstances(eventName, eventName);

			String headTable = eventName + "_head";
			String tailTable = eventName + "_tail";
			connection.dropTable(headTable);
			connection.dropTable(tailTable);
			connection.createTable(headTable);
			connection.createTable(tailTable);

			for(Instance i : all) {
				connection.insertInstance(SequenceUtils.generateHeadwithRatio(i, SolgrindConstants.H_R), headTable);
				connection.insertInstance(SequenceUtils.generateTailWindowWithRatio(i, SolgrindConstants.T_R, SolgrindConstants.BD, SolgrindConstants.TV), tailTable);
			}
		}
	}

	private void findCooccurences(String e1, String e2) {
		List<Instance> events1 = originalMap.get(e1);
		List<Instance> events2 = originalMap.get(e2);

		int count = 0;
		for(Instance i: events1) {
			for(Instance k: events2) {
				double jStar = new JaccardStar().calculate(i, k);
				InstanceVertex instanceVertex1 = new InstanceVertex(i.getId(), i.getType());
				InstanceVertex instanceVertex2 = new InstanceVertex(k.getId(), k.getType());
				
				if(jStar > 0 && jStar != 1) {
//					System.out.println("Jstar is :" + jStar);
					solgrindCooccurrence.addVertex(instanceVertex1);
					solgrindCooccurrence.addVertex(instanceVertex2);
					RelationEdge edge = solgrindCooccurrence.addEdge(instanceVertex1, instanceVertex2);
					if(edge != null) {
						edge.setRelation(RelationEdge.RelationType.COOCCURRENCE);
						solgrindCooccurrence.setEdgeWeight(edge, jStar);
						count++;
					}

				} else{
//					if(jStar != 0.0){
//						System.out.println("INVALID JSTAR!!!!!!!!! :" + jStar);						
//					}

					solgrindCooccurrence.addVertex(instanceVertex1);
					solgrindCooccurrence.addVertex(instanceVertex2);
				}
			}
		}
		System.out.println("I added " + count + " edges..");
	}


	/**
	 * Connects to database to pull all head and tails associated with the given event type
	 * Then, finds the intersecting head and tails to insert into graph
	 */
	private void findIntersectingHeadAndTails(Map<String, List<Instance>> headMap, Map<String, List<Instance>> tailMap, String headEventType, String tailEventType) {
		String headEventName = headEventType + "_head";
		String tailEventName = tailEventType + "_tail";

//		List<Instance> heads = SolgrindDBConnectionProvider.getSimpleDBConnection().getAllInstances(headEventName, headEventType);
//		List<Instance> tails = SolgrindDBConnectionProvider.getSimpleDBConnection().getAllInstances(tailEventName, tailEventType);

		List<Instance> heads = headMap.get(headEventName);
		List<Instance> tails = tailMap.get(tailEventName);
		for(Instance head: heads) {
			for(Instance tail: tails) {
				/**
				 * Check if head starts before tail and
				 * Check if there is a time overlap
				 *
				 * Then, check if there is a spatial overlap (calculate jaccard star)
				 */

				InstanceVertex tailVertex = new InstanceVertex(tail.getId(), tail.getType());
				InstanceVertex headVertex = new InstanceVertex(head.getId(), head.getType());
				solgrindSequence.addVertex(tailVertex);
				solgrindSequence.addVertex(headVertex);
				if(head.getStartTime() > eventIdByStartTime.get(tail.getId()) && head.getInterval().overlaps(tail.getInterval())) {
					double jaccardStar = 0;
					if((jaccardStar = new JaccardStar().calculate(head, tail)) > 0) {
						RelationEdge relationEdge = solgrindSequence.addEdge(tailVertex, headVertex);
						if(relationEdge != null) {
							relationEdge.setRelation(RelationEdge.RelationType.SEQUENCE);
							solgrindSequence.setEdgeWeight(relationEdge, jaccardStar);
						}
					}
				}
			}
		}


	}

	public void buildSimilarityGraph(){

	}
	
	public ArrayList<Set<InstanceVertex>> findKCooccurrenceCliques(int K){
		
		return this.getSolgrindCooccurrence().findKCliques(K);
		
	}

	public void constructSolgrind(){
		
	/*	solgrind.vertexSet().addAll(solgrindSequence.vertexSet());
		solgrind.edgeSet().addAll(solgrindSequence.edgeSet());

		solgrind.vertexSet().addAll(solgrindCooccurrence.vertexSet());
		solgrind.edgeSet().addAll(solgrindCooccurrence.edgeSet());
		
		solgrind.vertexSet().addAll(solgrindSimilarity.vertexSet());
		solgrind.edgeSet().addAll(solgrindSimilarity.edgeSet());*/


	}

	public SimilarityGraph getSolgrindSimilarity() {
		return solgrindSimilarity;
	}

	public CooccurrenceGraph getSolgrindCooccurrence() {
		return solgrindCooccurrence;
	}

	public SequenceGraph getSolgrindSequence() {
		return solgrindSequence;
	}

	public CooccurrenceGraph generateNullCooccurrenceModel(double pHat) {
		return this.getSolgrindCooccurrence().randomNullModel(pHat);
		
	}
}
