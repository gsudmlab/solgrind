package edu.gsu.dmlab.solgrind.index;

import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;

public class InstanceVertex {
	
	private String id;
	private EventType type;
	
	public InstanceVertex(String id, EventType type){
		this.setId(id);
		this.setType(new EventType(type.getType()));
	}

	public InstanceVertex(InstanceVertex v) {
		this.id = v.getId();
		this.type = new EventType(v.getType().getType());
	}
	
	public InstanceVertex getInstanceVertex(Instance ins){
		return new InstanceVertex(ins.getId(), ins.getType());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public EventType getType() {
		return type;
	}

	public void setType(EventType type) {
		this.type = type;
	}

	@Override
	public boolean equals(Object o){

		if(this == o) return true;

		if (!(o instanceof InstanceVertex)) {
			return false;
		}

		InstanceVertex iV = (InstanceVertex) o;

		return this.id.equalsIgnoreCase(iV.getId()) && this.type.equals(iV.getType());
	}

	@Override
	public int hashCode(){
		return this.type.getType().hashCode() + id.hashCode() ;
	}

	@Override
	public String toString() {
		return "InstanceVertex{" +
				"id='" + id + '\'' +
				", type=" + type +
				'}';
	}
}
