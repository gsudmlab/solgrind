package edu.gsu.dmlab.solgrind.index;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.math3.util.Combinations;
import org.jgrapht.Graph;

public class BronKerboschKCliqueFinder{

    private final Graph<InstanceVertex, RelationEdge> graph;

    private Collection<Set<InstanceVertex>> cliques;
	
	public BronKerboschKCliqueFinder(Graph<InstanceVertex, RelationEdge> graph) {
        this.graph = graph;
	}
	
	/**
     * Finds all maximal cliques of the graph. A clique is maximal if it is impossible to enlarge it
     * by adding another vertex from the graph. Note that a maximal clique is not necessarily the
     * biggest clique in the graph.
     *
     * @return Collection of cliques (each of which is represented as a Set of vertices)
     */
    public Collection<Set<InstanceVertex>> getAllMaximalCliques()
    {
        // TODO jvs 26-July-2005: assert that graph is simple

        cliques = new ArrayList<>();
        List<InstanceVertex> potential_clique = new ArrayList<>();
        List<InstanceVertex> candidates = new ArrayList<>();
        List<InstanceVertex> already_found = new ArrayList<>();
        candidates.addAll(graph.vertexSet());
        findCliques(potential_clique, candidates, already_found);
        return cliques;
    }

	/**
     * Finds k-cliques of the graph. A k-clique is a clique consisting of K vertices connected to each other. 
     *
     * @return Collection of cliques (each of which is represented as a Set of vertices)
     */
    public Collection<Set<InstanceVertex>> getKCliques(int K)
    {

        cliques = new ArrayList<>();
        List<InstanceVertex> potential_clique = new ArrayList<>();
        List<InstanceVertex> candidates = new ArrayList<>();
        List<InstanceVertex> already_found = new ArrayList<>();
        candidates.addAll(graph.vertexSet());
        findCliques(potential_clique, candidates, already_found);
        
        ArrayList<Set<InstanceVertex>> kcliques = new ArrayList<>();
        for (Iterator<Set<InstanceVertex>> iterator = cliques.iterator(); iterator.hasNext();) {
            Set<InstanceVertex> clique = iterator.next();
            if (clique.size() < K) { // if individual clique size less than K, remove that 
                iterator.remove();
            } else if (clique.size() == K){
            	kcliques.add(clique);
            } else if (clique.size() > K){
            	ArrayList<Set<InstanceVertex>> ksubsets = BronKerboschKCliqueFinder.kSubsets(clique, K);
            	kcliques.addAll(ksubsets);
            }
        }
        return kcliques;
    }


    private static ArrayList<Set<InstanceVertex>> kSubsets(Set<InstanceVertex> clique, int K) {
    	ArrayList<Set<InstanceVertex>> ksubsets = new ArrayList<Set<InstanceVertex>>();
    	
    	InstanceVertex[] cliqueArr = new InstanceVertex[clique.size()];
    	clique.toArray(cliqueArr);
    	
    	int cliqueSize = cliqueArr.length;
		for (Iterator<int[]> iter = new Combinations(cliqueSize, K).iterator(); iter.hasNext();) {
			
            System.out.println(Arrays.toString(iter.next()));
            int[] selection = iter.next();
            Set<InstanceVertex> kclique = new HashSet<InstanceVertex>();
            for (int i = 0; i < selection.length; i++){
            	InstanceVertex v = cliqueArr[selection[i]];
            	kclique.add(v);
            }
            ksubsets.add(kclique);
        }
		
		return ksubsets;
	}
    
    public static void subset(InstanceVertex[] cliqueArr, int k, int start, int currLen, boolean[] used) {

		if (currLen == k) {
			for (int i = 0; i < cliqueArr.length; i++) {
				if (used[i] == true) {
					System.out.print(cliqueArr[i] + " ");
				}
			}
			System.out.println();
			return;
		}
		if (start == cliqueArr.length) {
			return;
		}
		// For every index we have two options,
		// 1.. Either we select it, means put true in used[] and make currLen+1
		used[start] = true;
		subset(cliqueArr, k, start + 1, currLen + 1, used);
		// 2.. OR we dont select it, means put false in used[] and dont increase
		// currLen
		used[start] = false;
		subset(cliqueArr, k, start + 1, currLen, used);
	}

	private void findCliques(List<InstanceVertex> potential_clique, List<InstanceVertex> candidates, 
    		List<InstanceVertex> already_found) {
        List<InstanceVertex> candidates_array = new ArrayList<>(candidates);
        if (!end(candidates, already_found)) {
            // for each candidate_node in candidates do
            for (InstanceVertex candidate : candidates_array) {
                List<InstanceVertex> new_candidates = new ArrayList<>();
                List<InstanceVertex> new_already_found = new ArrayList<>();

                // move candidate node to potential_clique
                potential_clique.add(candidate);
                candidates.remove(candidate);

                // create new_candidates by removing nodes in candidates not
                // connected to candidate node
                for (InstanceVertex new_candidate : candidates) {
                    if (graph.containsEdge(candidate, new_candidate)) {
                        new_candidates.add(new_candidate);
                    } // of if
                } // of for

                // create new_already_found by removing nodes in already_found
                // not connected to candidate node
                for (InstanceVertex new_found : already_found) {
                    if (graph.containsEdge(candidate, new_found)) {
                        new_already_found.add(new_found);
                    } // of if
                } // of for

                // if new_candidates and new_already_found are empty
                if (new_candidates.isEmpty() && new_already_found.isEmpty()) {
                    // potential_clique is maximal_clique
                	
                    cliques.add(new HashSet<>(potential_clique));
                } // of if
                else {
                    // recursive call
                    findCliques(potential_clique, new_candidates, new_already_found);
                } // of else

                // move candidate_node from potential_clique to already_found;
                already_found.add(candidate);
                potential_clique.remove(candidate);
            } // of for
        } // of if
    }

    private boolean end(List<InstanceVertex> candidates, List<InstanceVertex> already_found)
    {
        // if a node in already_found is connected to all nodes in candidates
        boolean end = false;
        int edgecounter;
        for (InstanceVertex found : already_found) {
            edgecounter = 0;
            for (InstanceVertex candidate : candidates) {
                if (graph.containsEdge(found, candidate)) {
                    edgecounter++;
                } // of if
            } // of for
            if (edgecounter == candidates.size()) {
                end = true;
            }
        } // of for
        return end;
    }

	public static Collection<Set<InstanceVertex>> getKCliquesFromMaximal(int K,
			List<Set<InstanceVertex>> maximalCliques) {
		
		ArrayList<Set<InstanceVertex>> kcliques = new ArrayList<>();
        for (Iterator<Set<InstanceVertex>> iterator = maximalCliques.iterator(); iterator.hasNext();) {
            Set<InstanceVertex> clique = iterator.next();
            if (clique.size() < K) { // if individual clique size less than K, remove that 
                iterator.remove();
            } else if (clique.size() == K){
            	kcliques.add(clique);
            } else if (clique.size() > K){
            	ArrayList<Set<InstanceVertex>> ksubsets = BronKerboschKCliqueFinder.kSubsets(clique, K);
            	kcliques.addAll(ksubsets);
            }
        }
        return kcliques;
	}
    


}
