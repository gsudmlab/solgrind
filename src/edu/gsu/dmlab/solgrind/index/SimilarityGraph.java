package edu.gsu.dmlab.solgrind.index;

import org.jgrapht.graph.SimpleWeightedGraph;

public class SimilarityGraph extends SimpleWeightedGraph<InstanceVertex, RelationEdge> {

	/**
	 * generated graph identifier
	 */
	private static final long serialVersionUID = 5772434644021492789L;

	public SimilarityGraph(Class<? extends RelationEdge> edgeClass) {
		super(edgeClass);
	}

}
