package edu.gsu.dmlab.solgrind.index;

import org.jgrapht.graph.DefaultWeightedEdge;


/**
 * 
 * @author berkay - Jul 28, 2016
 *
 */

public class RelationEdge extends DefaultWeightedEdge {

	/**
	 * Generated serial id
	 */
	private static final long serialVersionUID = -8918765472039977390L;

	public enum RelationType {
		COOCCURRENCE, SEQUENCE, SIMILARITY
	}

	private RelationType relation;

	public RelationEdge() {
		super();
	}

	public RelationEdge(RelationType type) {
		super();
		relation = type;
	}

	public RelationType getRelation() {
		return relation;
	}

	public void setRelation(RelationType relation) {
		this.relation = relation;
	}

	public void setRelation(String relation) {
		if(relation.equalsIgnoreCase("SEQUENCE")) {
			this.relation = RelationType.SEQUENCE;
		} else if(relation.equalsIgnoreCase("COOCCURRENCE")) {
			this.relation = RelationType.COOCCURRENCE;
		} else if(relation.equalsIgnoreCase("SIMILARITY")) {
			this.relation = RelationType.SIMILARITY;
		} else {
			System.err.println("Edge relation cannot be found");
		}
	}

	public double getWeight() {
		return super.getWeight();
	}

	public String toString() {
		if(relation == null) return "weight: " + getWeight();
		String str = "";
		switch (relation) {
		case COOCCURRENCE:
			str = "co[" + getWeight() + "]";
			break;
		case SEQUENCE:
			str = "sq[" + getWeight() + "]";
			break;
		case SIMILARITY:
			str = "sm[" + getWeight() + "]";
			break;
		}
		return str;
	}

}
