package edu.gsu.dmlab.solgrind.index;

import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.SimpleWeightedGraph;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class CooccurrenceGraph extends SimpleWeightedGraph<InstanceVertex, RelationEdge> implements WeightedGraph<InstanceVertex, RelationEdge>, Serializable{

	/**
	 * generated serial version uid
	 */
	private static final long serialVersionUID = -8686250470583163430L;
	
	private List<Set<InstanceVertex>> maximalCliques = null;

	public CooccurrenceGraph(Class<? extends RelationEdge> edgeClass) {
		super(edgeClass);
	}
	
	public List<Set<InstanceVertex>> getMaximalCliques(){
		if(maximalCliques == null){
			BronKerboschKCliqueFinder cliqueFinder = new BronKerboschKCliqueFinder(this);
			maximalCliques = new ArrayList<>(cliqueFinder.getAllMaximalCliques() );
		}
		return maximalCliques;
	}
	
	public ArrayList<Set<InstanceVertex>> findKCliques(int K){
//		a = new BronKerboschKCliqueFinder(this.getSolgrindCooccurrence());
		
		Collection<Set<InstanceVertex>> kclique_set = 
				BronKerboschKCliqueFinder.getKCliquesFromMaximal(K, this.getMaximalCliques());
		return new ArrayList<Set<InstanceVertex>>(kclique_set);
		
	}

	public CooccurrenceGraph randomNullModel(double pHat) {
		CooccurrenceGraph sampledGraph = new CooccurrenceGraph(RelationEdge.class);
		
		for( InstanceVertex instanceVertex : this.vertexSet()){
			sampledGraph.addVertex(instanceVertex);
		}
		
		Iterator<InstanceVertex> vertexIterator1 = sampledGraph.vertexSet().iterator();
		
		int count = 0;
		while(vertexIterator1.hasNext()){
			InstanceVertex v1 = vertexIterator1.next();
			Iterator<InstanceVertex> vertexIterator2 = sampledGraph.vertexSet().iterator();
			while(vertexIterator2.hasNext()){
				InstanceVertex v2 = vertexIterator2.next();
				
				if( ! v1.getType().equals(v2.getType()) ){ // if the instances are from the same event type continue
					count++;
					double randProb = ThreadLocalRandom.current().nextDouble(0.0, 1.0);
					if (pHat > randProb){
						//add edge
//						System.out.println("Random probability: "+ randProb + " pHat was " + pHat );
						RelationEdge relationEdge = sampledGraph.addEdge(v1, v2);
						if(relationEdge != null) {
							relationEdge.setRelation(RelationEdge.RelationType.COOCCURRENCE);
							sampledGraph.setEdgeWeight(relationEdge, 0.9999);
						}
					}
				}
				
			}
		}
		System.out.println("Number of trials: " + count );
		System.out.println("pHat: " + pHat );
		System.out.println("Expected number of edges: " + (count*pHat));
		System.out.println("Number of edges: " + sampledGraph.edgeSet().size());
		
		return sampledGraph;
	}
	
	
	
}
