package edu.gsu.dmlab.solgrind;

public class SolgrindConstants {
	
	public static final String DATASET_DIR = "/Users/ahmetkucuk/Documents/Research/BERKAY/1Mo_out/";


//	public static final String[] EVENT_TYPES = new String[]{"ar", "ch", "fi", "ss", "ef", "sg"};

	/*
	 * Event Co-occurrence parameters
	 */
	//cce threshold
	public static final double CCE_th = 0.00;
	//pi threshold
//	public static final double PI_th = 0.01;

	public static long SAMPLING_INTERVAL = 10*60*1000; // in milliseconds now

	/*
	 * Event EventEventSequence parameters
	 */
	//head interval (in seconds)
	public static final long H_in = 24; //3600 seconds is an hour
	public static final double H_R = 0.10; 
	//tail interval (in seconds)
	public static final long T_in = 24;
	public static final double T_R = 0.20;
	//head interval (in seconds)
	public static final long TV = 12; // Tail validity interval is (TV * SAMPLING_INTERVAL) milliseconds
	//buffer distance (in arcsec)
	public static final double BD = 10;
	//ci threshold
	public static final double CI_th = 0.01;

}
