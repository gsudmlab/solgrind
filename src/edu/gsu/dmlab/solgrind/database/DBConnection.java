package edu.gsu.dmlab.solgrind.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class DBConnection {

	static Connection dbConn;
		
	private DBConnection() throws SQLException{
		if (dbConn == null){
			dbConn = getConnection();
		} else if (dbConn.isClosed()){
			dbConn = getConnection();
		}
	}
	
	public static Connection getConnection() throws SQLException{
		String dbPassword="xxxxxxx";
		String dbUser="postgres";
		String dbURL="jdbc:postgresql://localhost/stesdb?allowMultiQueries=true";
		
		
        Properties props = new Properties();
        props.setProperty("user", dbUser);
        props.setProperty("password", dbPassword);
        props.setProperty("ssl", "true");
        dbConn = DriverManager.getConnection(dbURL, props);
		
		return dbConn;
		
	}
	
	

}
