package edu.gsu.dmlab.solgrind.database;

import java.util.Set;

import edu.gsu.dmlab.solgrind.base.types.instance.InstanceCooccurrence;
import edu.gsu.dmlab.solgrind.base.types.instance.InstanceSequence;
import edu.gsu.dmlab.solgrind.base.types.instance.InstanceSimilarity;

public interface iDBUtil {

	
	public Set<InstanceSequence> discoverInstanceSequences();
	public Set<InstanceCooccurrence> discoverInstanceCooccurrences();
	public Set<InstanceSimilarity> discoverInstanceSimilarities();
	
	
}
