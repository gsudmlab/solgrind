package edu.gsu.dmlab.solgrind.database;

import edu.gsu.dmlab.solgrind.database.interfaces.SolgrindDBConnection;
import org.postgresql.ds.PGPoolingDataSource;

import javax.sql.DataSource;

/**
 * Created by ahmetkucuk on 19/08/16.
 */
public class SolgrindDBConnectionProvider {

//    private static SolgrindDBConnection connection = new SolgrindDBConnectionPostgres(getDefaultDataSource());
    private static SolgrindDBConnection simpleConnection = new SolgrindDBConnectionSimple(getDefaultDataSource());

    public static SolgrindDBConnection getConnection() {
        return simpleConnection;
    }

    public static SolgrindDBConnection getSimpleDBConnection() {
        return simpleConnection;
    }

    private static DataSource getDefaultDataSource() {
        PGPoolingDataSource source = new PGPoolingDataSource();
        source.setDataSourceName("A Data Source");
        source.setServerName("localhost");
        source.setDatabaseName("one_month");
        source.setUser("ahmetkucuk");
        source.setPassword("");
        source.setMaxConnections(10);
        return source;
    }
}
