package edu.gsu.dmlab.solgrind.database.query;

import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.types.instance.InstanceCooccurrence;
import edu.gsu.dmlab.solgrind.base.types.instance.InstanceSequence;
import edu.gsu.dmlab.solgrind.base.types.instance.InstanceSimilarity;
import edu.gsu.dmlab.solgrind.database.iDBUtil;
import edu.gsu.dmlab.solgrind.index.*;
import org.jgrapht.Graphs;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 
 * @author berkay - Aug 1, 2016
 *
 */

public class QueryUtil implements iDBUtil{

	@Override
	public Set<InstanceSequence> discoverInstanceSequences() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<InstanceCooccurrence> discoverInstanceCooccurrences() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<InstanceSimilarity> discoverInstanceSimilarities() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * This method designed for following query
	 *
	 * Find co1 (AR) that cooccurs with a co2 (SS) and are followed by a follows (SG)
	 * @param graphIndex
	 * @param co1
	 * @param co2
	 * @param follows
     * @return
     */
	public static Set<InstanceVertex> findCooccurAndFollowedBy(GraphIndex graphIndex, EventType co1, EventType co2, EventType follows) {
		CooccurrenceGraph cooccurrenceGraph = graphIndex.getSolgrindCooccurrence();
		SequenceGraph sequenceGraph = graphIndex.getSolgrindSequence();

		Set<InstanceVertex> allCo1 = cooccurrenceGraph.vertexSet().stream().filter(v -> v.getType().equals(co1)).collect(Collectors.toSet());

		Set<InstanceVertex> cooccuringCo1 = new HashSet<>();
		for(InstanceVertex v: allCo1) {
			if(Graphs.neighborListOf(cooccurrenceGraph, v).stream().filter(v2 -> v2.getType().equals(co2)).count() > 0) {
				cooccuringCo1.add(v);
			}
		}

		Set<InstanceVertex> resultingEvents = new HashSet<>();
		for(InstanceVertex v: cooccuringCo1) {
			if(sequenceGraph.containsVertex(v)) {
				if(Graphs.successorListOf(sequenceGraph, v).stream().filter(f -> f.getType().equals(follows)).count() > 0) {
					resultingEvents.add(v);
				}
			}
		}
		return resultingEvents;
	}

	/**
	 * This method find cooccurences of event with weight greater than given threshold
	 * @param graphIndex
	 * @param e1
	 * @param e2
	 * @param threshold
     * @return
     */
	public static Set<InstanceVertex> findCooccurWith(GraphIndex graphIndex, EventType e1, EventType e2, double threshold) {

		CooccurrenceGraph cooccurrenceGraph = graphIndex.getSolgrindCooccurrence();

		Set<InstanceVertex> type1Set = cooccurrenceGraph.vertexSet().stream().filter(v -> v.getType().equals(e1)).collect(Collectors.toSet());

		Set<InstanceVertex> resultingEvents = new HashSet<>();
		for(InstanceVertex v: type1Set) {
			if(Graphs.neighborListOf(cooccurrenceGraph, v).stream().anyMatch(n -> {
				if(n.getType().equals(e2)) {
					RelationEdge r = cooccurrenceGraph.getEdge(v, n);
					if(cooccurrenceGraph.getEdgeWeight(r) > threshold) return true;
				}
				return false;
			})) {
				resultingEvents.add(v);
			}
		}
		return resultingEvents;
	}


	public static Set<InstanceVertex> findFollowedBy(GraphIndex graphIndex, EventType followed, EventType followers, double threshold) {
		SequenceGraph sequenceGraph = graphIndex.getSolgrindSequence();

		Set<InstanceVertex> followedSet = sequenceGraph.vertexSet().stream().filter(v -> v.getType().equals(followed)).collect(Collectors.toSet());

		Set<InstanceVertex> resultingEvents = new HashSet<>();
		for(InstanceVertex i: followedSet) {
			if(Graphs.successorListOf(sequenceGraph, i).stream().allMatch(v -> {
				if(v.getType().equals(followers)) {
					RelationEdge relationEdge = sequenceGraph.getEdge(i, v);
					if(sequenceGraph.getEdgeWeight(relationEdge) > threshold) {
						return true;
					}
				}
				return false;
			})) {
				resultingEvents.add(i);
			}
		}
		return resultingEvents;
	}
	
	
}
