package edu.gsu.dmlab.solgrind.algo.stesminers;

import edu.gsu.dmlab.solgrind.base.types.event.EventSequence;
import edu.gsu.dmlab.solgrind.index.SequenceGraph;

import java.util.*;

/**
 * Created by ahmetkucuk on 07/10/16.
 */
public class BtspESMiner {

    private Map<EventSequence, List<Double>> results;

    /**
     * Given an event sequence graph (esGraph), BtspESMiner randomly resamples the sequence edges
     * in the graph, and simply creates a subgraph. sampleRatio shows the fraction of edges to be resampled
     * bootstrapCount shows how many times the mining is performed.
     * @param graph
     * @param resampleRatio
     * @param bootstrapCount
     */
    public BtspESMiner(SequenceGraph graph, double resampleRatio, int bootstrapCount) {

        results = new HashMap<>();

        for(int i = 0; i < bootstrapCount; i++) {
            SequenceGraph sampledGraph = graph.randomSampleEdges(resampleRatio);
            Map<EventSequence, Double> map = new ESGrowthMiner(sampledGraph, 0).getResultInMap();
            for(Map.Entry<EventSequence, Double> entry: map.entrySet()) {
                results.putIfAbsent(entry.getKey(), new ArrayList<>());
                results.get(entry.getKey()).add(entry.getValue());
            }
        }

        for(EventSequence e : results.keySet()) {
            List<Double> tempList = results.get(e);
            while(tempList.size() < bootstrapCount) tempList.add(0.0);
        }
        System.out.println("Found " + results.size() + " event sequences from " + bootstrapCount + " bootstrap runs..");
        System.out.println("==END OF MINING (BTSP-ESMiner)");
    }

    public Map<EventSequence, List<Double>> getResults() {
        return results;
    }
}
