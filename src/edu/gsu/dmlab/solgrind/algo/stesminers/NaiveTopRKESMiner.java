package edu.gsu.dmlab.solgrind.algo.stesminers;

import edu.gsu.dmlab.solgrind.base.types.event.EventSequence;
import edu.gsu.dmlab.solgrind.index.RelationEdge;
import edu.gsu.dmlab.solgrind.index.SequenceGraph;

import java.util.*;

/**
 * Created by ahmetkucuk on 3/12/17.
 */
public class NaiveTopRKESMiner {

    private Set<EventSequence> resultSet = new HashSet<>();
    private Map<EventSequence, Double> resultMap = new HashMap<>();
    private ESGrowthMiner esGrowthMiner;

    public NaiveTopRKESMiner(SequenceGraph esGraph, double rPercent, int K) {
        esGraph.rPercentFilter(rPercent);
        this.esGrowthMiner = new ESGrowthMiner(esGraph, 0);
        collectTopK(K);
        System.out.println("==END OF MINING (Naive Top-(R%, K) Miner)");
    }

    private void collectTopK(int K) {

        List<EventSequence> all = new ArrayList<>(esGrowthMiner.getResultSet());
        Collections.sort(all, EventSequence.piComparator);
        
        resultSet.addAll(all.subList(Math.max(all.size()-K, 0), all.size()));
        double minPI = all.get(Math.max(all.size()-K, 0) ).getPiValue();
        for(EventSequence e: resultSet) {
            resultMap.put(e, e.getPiValue());
        }
        System.out.println("Found Top-" + resultSet.size() + " STESs.. Min pi=" + minPI );
    }

    public Set<EventSequence> getResultSet() {
        return resultSet;
    }


    public Map<EventSequence, Double> getResultInMap() {
        return resultMap;
    }

}
