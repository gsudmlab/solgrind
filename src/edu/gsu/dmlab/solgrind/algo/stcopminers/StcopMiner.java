package edu.gsu.dmlab.solgrind.algo.stcopminers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import edu.gsu.dmlab.solgrind.algo.measures.significance.JaccardStar;
import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.operations.STOperations;
import edu.gsu.dmlab.solgrind.base.types.essential.TInterval;
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;
import edu.gsu.dmlab.solgrind.base.types.event.EventCooccurrence;
import edu.gsu.dmlab.solgrind.base.types.instance.InstanceCooccurrence;
import edu.gsu.dmlab.solgrind.base.types.instance.InstanceData;

/**
 * Spatiotemporal co-occurrence pattern mining discovers 
 * sets of event types whose instances frequently co-occur
 * in both space and time.
 * 
 * @author berkay - Jul 12, 2016
 *
 */
public class StcopMiner {

	static double CCEth = 0.0;//0.00000001;
	static double PIth = 0.001;
	
	static Map<EventCooccurrence, ArrayList<InstanceCooccurrence>> instanceCooccurrenceMap = null;
	static Map<EventType, Integer> instanceCountMap = null;
	static Map<EventType, Map<String, Instance>> instanceMap = null;
	
	public StcopMiner( Map<EventType, Set<Instance>> instanceSetMap){
		StcopMiner.instanceMap = transformMap(instanceSetMap);
		instanceCooccurrenceMap = new HashMap<EventCooccurrence, ArrayList<InstanceCooccurrence>>();
		instanceCountMap = countInstances(instanceSetMap);
		
		Set<EventType> eventTypes = instanceSetMap.keySet();
		
		
		ArrayList<EventCooccurrence> S2_CP = generateSize2CandidateSTCOPs(eventTypes);
		ArrayList<EventCooccurrence> S2_PP = discoverSize2STCOPs( S2_CP, instanceSetMap );
		ArrayList<EventCooccurrence> prevPatterns = S2_PP;
		//System.out.println("Size 2 Patterns==>>\n" + S2_PP);
		while(prevPatterns != null && !prevPatterns.isEmpty()){
			
			ArrayList<EventCooccurrence> SK_CP = generateSizeKCandidateSTCOPs(prevPatterns);
			System.out.println("Size-K Candidates: " + SK_CP);
			ArrayList<EventCooccurrence> SK_PP = discoverSizeKSTCOPs( SK_CP, instanceCooccurrenceMap);
			prevPatterns = SK_PP;
		}
	}
	
	public StcopMiner( Map<EventType, Set<Instance>> instanceSetMap, double cceTh, double piTh){
		StcopMiner.CCEth = cceTh;
		StcopMiner.PIth = piTh;
		
		StcopMiner.instanceMap = transformMap(instanceSetMap);
		instanceCooccurrenceMap = new HashMap<EventCooccurrence, ArrayList<InstanceCooccurrence>>();
		instanceCountMap = countInstances(instanceSetMap);
		
		Set<EventType> eventTypes = instanceSetMap.keySet();
		
		
		ArrayList<EventCooccurrence> S2_CP = generateSize2CandidateSTCOPs(eventTypes);
		ArrayList<EventCooccurrence> S2_PP = discoverSize2STCOPs( S2_CP, instanceSetMap );
		ArrayList<EventCooccurrence> prevPatterns = S2_PP;
		//System.out.println("Size 2 Patterns==>>\n" + S2_PP);
		while(prevPatterns != null && !prevPatterns.isEmpty()){
			
			ArrayList<EventCooccurrence> SK_CP = generateSizeKCandidateSTCOPs(prevPatterns);
			System.out.println("Size-K Candidates: " + SK_CP);
			ArrayList<EventCooccurrence> SK_PP = discoverSizeKSTCOPs( SK_CP, instanceCooccurrenceMap);
			prevPatterns = SK_PP;
		}
	}

	
	/**
	 * Default constructor for creating empty objects
	 */
	public StcopMiner() {
	}



	protected Map<EventType, Map<String, Instance>> transformMap(Map<EventType, Set<Instance>> instanceSetMap) {
		HashMap<EventType, Map<String, Instance>> instanceMap = new HashMap<>();
		for(EventType et : instanceSetMap.keySet()){
			
			instanceMap.put(et, new HashMap<String, Instance>());
//			System.out.println("Adding " + et);
//			System.out.println("There are " + instanceSetMap.get(et).size() + " instances originally");
			
			for(Instance ins : instanceSetMap.get(et)){
				instanceMap.get(et).put( ins.getId(), ins);
			}
//			System.out.println("I added " + instanceMap.get(et).size() + " in the end" );
		}
		return instanceMap;
	}



	protected ArrayList<EventCooccurrence> discoverSizeKSTCOPs(ArrayList<EventCooccurrence> sK_CP,
			Map<EventCooccurrence, ArrayList<InstanceCooccurrence>> patternInstanceMap2) {
		// TODO this is still not finished
		ArrayList<EventCooccurrence> sk_pp = new ArrayList<>();

		for (EventCooccurrence candidate : sK_CP){
			
			ArrayList<EventCooccurrence> subsets = candidate.getImmediateSubsets();
			EventCooccurrence subset1 = subsets.get(0);
			EventCooccurrence subset2 = subsets.get(subsets.size()-1);
			
			ArrayList<InstanceCooccurrence> icSet1 = instanceCooccurrenceMap.get(subset1);
			ArrayList<InstanceCooccurrence> icSet2 = instanceCooccurrenceMap.get(subset2);
			
			ArrayList<InstanceCooccurrence> significantCOs = findSizeKCooccurences(icSet1, icSet2);
			
			double pi = calculatePi(candidate, significantCOs);
			if(pi > StcopMiner.PIth){
				System.out.println("Candidate passed the pi threshold " + candidate + " PI:" + pi);
				instanceCooccurrenceMap.put(candidate, significantCOs);
				sk_pp.add(candidate);
			} else{
				System.out.println("Candidate cannot pass the pi threshold " + candidate + " PI:" + pi);
			}
		}
		return sk_pp;
	}






	protected ArrayList<EventCooccurrence> generateSizeKCandidateSTCOPs(ArrayList<EventCooccurrence> prevPatterns) {
		Collections.sort(prevPatterns);
		System.out.println(prevPatterns);

        ArrayList<EventCooccurrence> currentCandidates = new ArrayList<EventCooccurrence>();
        
        int prevCardinality = prevPatterns.get(0).getCardinality();

        for (int i = 0; i < prevPatterns.size(); i++) {
            EventCooccurrence pattern = new EventCooccurrence(prevPatterns.get(i));
            TreeSet<EventType> patternEventTypes = new TreeSet<EventType>(pattern.getEventTypes()) ;
            EventType firstET= patternEventTypes.first();
            EventType lastET= patternEventTypes.last();

            TreeSet<EventType> query = (TreeSet<EventType>) patternEventTypes.subSet(firstET, false, lastET, true);
//            System.out.println("Query" + query);
            for (int j = i+1; j < prevPatterns.size(); j++) {
                EventCooccurrence p = new EventCooccurrence(prevPatterns.get(j));
                TreeSet<EventType> pFeatures = new TreeSet<>(  p.getEventTypes() );
                EventType pLastElement = pFeatures.pollLast();

//                System.out.println("\tpFeatures" + pFeatures);
//                System.out.println("\tLast Element:" + pLastElement);
                
                if (pFeatures.size() == 0) { // it means the prev cardinality is 1, we should be good
                    EventCooccurrence t = new EventCooccurrence();
                    t.addEventType(firstET);
                    t.addEventType(pLastElement);
                    currentCandidates.add(t);
                } else { // it means we need to check if pFeatures and query is
                    // the same
                    if (query.containsAll(pFeatures)) {
                        // then we need to check for other possible subsets
                        EventCooccurrence possibleCandidate = pattern.union(prevPatterns.get(j));
//                        System.out.println("\t\tPossible Cand:" + possibleCandidate);
                        ArrayList<EventType> pcEventTypes = new ArrayList<EventType>(possibleCandidate.getEventTypes());
                        boolean isRealCandidate = true;
                        for (int s = 1; s < pcEventTypes.size() - 1; s++) { // search for other subsets
                            ArrayList<EventType> eventTypeList = new ArrayList<EventType>(pcEventTypes);
                            eventTypeList.remove(s); // now: eventTypeList is one of the other
                            // subsets that must be present in the previous candidate list
//                            System.out.println("\t\t\tEvent Type List: " + eventTypeList);
                            boolean isSubsetsExist = false;

                            for (int k = 0; k < prevPatterns.size(); k++) {
//                            	System.out.println("\t\t\t\tPrevPattern Iteration:" + prevPatterns.get(k).getEventTypes());
                                if (prevPatterns.get(k).getEventTypes().containsAll(eventTypeList)) {
                                    isSubsetsExist = true;
                                    break;
                                }
                            }
                            if (!isSubsetsExist) {// if it doesn't exist, then
                                // it is not a candidate
                                isRealCandidate = false;
                                break;
                            }
                        }
                        if (isRealCandidate) {
                            currentCandidates.add(possibleCandidate);
                        }
                    }
                }
            }
        }
        if (currentCandidates.size() != 0) {
            System.out.println(":::" + (prevCardinality + 1) + "-cardinality candidates:::");
        }

        for (EventCooccurrence t : currentCandidates) {
        	System.out.println(t + "\n");
        }

        return currentCandidates;
		
	}


	/**
	 * This method discovers size-2 STCOPs from given size-2 candidate patterns
	 * and the provided instance map. The event types in instance map and the participating
	 * event types in the candidate pattern list must match.
	 * @param s2_CP -- size-2 candidadate patterns 
	 * @param instanceMap -- instance map for each event type.
	 * @return - an array list of STCOPs (in the form of EventCooccurrence objects).
	 */
	protected ArrayList<EventCooccurrence> discoverSize2STCOPs(ArrayList<EventCooccurrence> s2_CP,
			Map<EventType, Set<Instance>> instanceMap) {
		ArrayList<EventCooccurrence> s2_pp = new ArrayList<>();
		for(EventCooccurrence size2Candidate : s2_CP){
			
			ArrayList<EventType> eventTypes = new ArrayList<EventType>(size2Candidate.getEventTypes());
			
			
			Set<Instance> e1Instances = instanceMap.get(eventTypes.get(0));
			Set<Instance> e2Instances = instanceMap.get(eventTypes.get(1));
			
			ArrayList<InstanceCooccurrence> significantCOs = findCooccurences(e1Instances, e2Instances);
			double pi = calculatePi(size2Candidate, significantCOs);
			if(pi > StcopMiner.PIth){
				System.out.println("Candidate passed the pi threshold " + size2Candidate + " PI:" + pi);
				size2Candidate.setPrevalence(pi);
				instanceCooccurrenceMap.put(size2Candidate, significantCOs);
				s2_pp.add(size2Candidate);
			}
		}
		
		return s2_pp;
	}

	protected double calculatePi(EventCooccurrence candidate, ArrayList<InstanceCooccurrence> cooccurrences) {
		HashMap<EventType, Set<String>> participatingInstanceMap = new HashMap<>();
		for(EventType et: candidate.getEventTypes()){
			participatingInstanceMap.put(et, new TreeSet<>());
		}
		
		for(InstanceCooccurrence cooccurrence : cooccurrences){
			for(EventType et : candidate.getEventTypes()){
				String id = cooccurrence.getInstanceIdOfType(et);
				participatingInstanceMap.get(et).add(id);
			}
		}
		double pi = Double.MAX_VALUE;
		for(Entry<EventType, Set<String>> kv : participatingInstanceMap.entrySet()){
			double total = instanceCountMap.get(kv.getKey());
			double participatorCount = participatingInstanceMap.get(kv.getKey()).size();
			double pr = participatorCount/total; // participation ratio
			if(pr < pi){
				pi = pr;
			}
		}
		if(pi == Double.MAX_VALUE){
			System.out.println("Warning invalid pi for" + candidate + " returning pi=-1.0");
			return -1.0;
		} else{
			return pi;
		}
	}


	/**
	 * Find ST co-occurrences between two sets of instances
	 * @param e1Instances - instances of event type e1
	 * @param e2Instances - instances of event type e2
	 * @return An ArrayList of pattern instances (in the form of InstanceCooccurrence objects)
	 */
	protected ArrayList<InstanceCooccurrence> findCooccurences(Set<Instance> e1Instances, 
			Set<Instance> e2Instances) {

		ArrayList<InstanceCooccurrence> overlappingCooccurrences = new ArrayList<>();
		for(Instance e1i: e1Instances) {
			for(Instance e2i: e2Instances) {
				InstanceCooccurrence ic = findCooccurrence(e1i, e2i);
				if(ic.isCceValid()){
					//System.out.println("Overlapping ST CO: " + ic );
					overlappingCooccurrences.add(ic);
				}
			}
		}
		return overlappingCooccurrences;
	}
	
	
	/**
	 * Find ST co-occurrences between two sets of instance co-occurrences
	 * @param icSet1 - instance co-occurrences of event co-occurrence ec1 (ec1 is a size(k-1) STCOP)
	 * @param icSet2 - instance co-occurrences of event co-occurrence ec2 (ec2 is a size(k-1) STCOP)
	 * @return 
	 * @return An ArrayList of pattern instances (in the form of InstanceCooccurrence objects)
	 */
	private ArrayList<InstanceCooccurrence> findSizeKCooccurences(ArrayList<InstanceCooccurrence> icSet1, 
																ArrayList<InstanceCooccurrence> icSet2) {
		ArrayList<InstanceCooccurrence> overlappingCooccurrences = new ArrayList<>();
		
		Set<EventType> ecSet1 = icSet1.get(0).getEventCooccurrenceType().getEventTypes();
		Set<EventType> ecSet2 = icSet2.get(0).getEventCooccurrenceType().getEventTypes();
		HashSet<EventType> ecsetUnion = new HashSet<EventType>(ecSet1);
		HashSet<EventType> ecsetIntersection = new HashSet<EventType>(ecSet1);
		ecsetUnion.addAll(ecSet2);
		ecsetIntersection.retainAll(ecSet2);

//		System.out.println(ecSet1);
//		System.out.println(ecSet2);
//		System.out.println(ecsetUnion);
//		System.out.println(ecsetIntersection);
		
		for(InstanceCooccurrence ic1: icSet1) {
			for(InstanceCooccurrence ic2: icSet2) {
				
				HashSet<Instance> instances = new HashSet<>();
				HashSet<InstanceData> unionSet = new HashSet<InstanceData>(ic1.getInstanceData());
				unionSet.addAll(ic2.getInstanceData());
				for(InstanceData idata : unionSet ){
					Instance temp = instanceMap.get(idata.type).get(idata.id);
					instances.add(temp);
				}
				

				if(instances.size() != ecsetUnion.size()){
					continue;
				} 
//				else{
//					System.out.print("Candidate Instance Co-occurrence:: ");
//					for(Instance instance : instances){
//						System.out.print(instance.getId() + " " + instance.getType() + "\t");
//					}
//					System.out.println();
//				}
				
				InstanceCooccurrence ic = findCooccurrence(instances);
				if(ic.isCceValid()){
					System.out.println("Overlapping ST CO: " + ic );
					overlappingCooccurrences.add(ic);
				}else{
//					System.out.println("IC cannot pass threshold: CCE--" + ic.getCce());
				}
			}
		}
		System.out.println("I have found " + overlappingCooccurrences.size() + " ICs for the EC: " + ecsetUnion);
		return overlappingCooccurrences;
	}

	
	/**
	 * Given two instances ins1 and ins2, this method creates an InstanceCoocurrence
	 * object (which may or may not be empty). The empty object corresponds to no pattern
	 * instance, and it is invalid. The non-empty ones are calculated.
	 * USE THIS WITH CAUTION, THIS IS AN EXPENSIVE OPERATION
	 * 
	 * @param ins1 - instance 1 to be checked
	 * @param ins2 - instance 2 to be checked
	 * @return a pattern instance (or an empty cooccurrence)
	 */
	protected InstanceCooccurrence findCooccurrence(Instance ins1, Instance ins2) {
		InstanceCooccurrence ic = new InstanceCooccurrence();
		if(!STOperations.stIntersects(ins1.getTrajectory(), ins2.getTrajectory())){
			return ic; //return empty ic
		} else{
			double jStar = new JaccardStar().calculate(ins1, ins2);
//			System.out.println("jstar is :" + jStar + " CCE::" + StcopMiner.CCEth);
			if(jStar <= StcopMiner.CCEth){
				return ic; // return empty ic
			} else {
//				Trajectory uTraj = STOperations.union(traj1, traj2);
//				Trajectory iTraj = STOperations.intersection(traj1, traj2);
//				TreeSet<TInterval> xcoIntervals = iTraj.getTimeIntervals();
				
				ic.append(new InstanceData(ins1));
				ic.append(new InstanceData(ins2));
//				ic.setIntersectionTrajectory(iTraj);
//				ic.setUnionTrajectory(uTraj);
//				ic.setXcoTimeIntervals(xcoIntervals);
				ic.setCce(jStar);
				ic.setcceType("J*");
				
				return ic;
			}
		}

//		double jStar = new JaccardStar().calculate(ins1, ins2);
//		
//		if(jStar <= StcopMiner.CCEth){
//			return ic; // return empty ic
//		} else {
////			System.out.println("jstar is :" + jStar + " CCE::" + StcopMiner.CCEth);
//			ic.append(new InstanceData(ins1));
//			ic.append(new InstanceData(ins2));
//			ic.setCce(jStar);
//			ic.setcceType("J*");
//			
//			return ic;
//		}
	
	}
	
	/**
	 * Given two instances ins1 and ins2, this method creates an InstanceCoocurrence
	 * object (which may or may not be empty). The empty object corresponds to no pattern
	 * instance, and it is invalid. The non-empty ones are calculated.
	 * USE THIS WITH CAUTION, THIS IS AN EXPENSIVE OPERATION
	 * 
	 * @param ins1 - instance 1 to be checked
	 * @param ins2 - instance 2 to be checked
	 * @return a pattern instance (or an empty cooccurrence)
	 */
	private InstanceCooccurrence findCooccurrence(Set<Instance> instances) {
		InstanceCooccurrence ic = new InstanceCooccurrence();
		
		ArrayList<Instance> instancesList = new ArrayList<>();
		// try a new naive intersection check (checking cliques of pairwise co-occurrences)
		for( int i = 0; i < instancesList.size(); i++ ){
			for(int j = 0; j < instancesList.size(); j++){
				if( i != j){
					boolean intersects = STOperations.stIntersects(
							instancesList.get(i).getTrajectory(), instancesList.get(j).getTrajectory());
					if(!intersects){
						return ic; // return the empty instance co-occurrence
					}
				}
			}
		}
		
		double jstar = new JaccardStar().calculate(instances);
		if(jstar <= StcopMiner.CCEth){
			return ic; // return empty instance co-occurrence
		} else{
			for(Instance ins : instances){
				ic.append(new InstanceData(ins));
			}
			ic.setCce(jstar);
			ic.setcceType("J*");
		}
		return ic;
	}

	/**
	 * This generates size-2 candidate patterns
	 * @param eventTypes: set of event types
	 * @return an arraylist of candidates to be examined
	 */
	protected ArrayList<EventCooccurrence> generateSize2CandidateSTCOPs(Set<EventType> eventTypes) {
		ArrayList<EventCooccurrence> size2Candidates = new ArrayList<>();
		ArrayList<EventType> etList = new ArrayList<>(eventTypes);
		for(int i = 0; i < etList.size(); i++){
			for(int j = i+1; j < etList.size(); j++){
				EventCooccurrence candidate = new EventCooccurrence();
				candidate.addEventType(etList.get(i));
				candidate.addEventType(etList.get(j));
				size2Candidates.add(candidate);
			}
		}
		System.out.println("Size-2 Candidates:");
		for(EventCooccurrence candidate : size2Candidates){
			System.out.println(candidate);
		}
		return size2Candidates;
	}
	
	/**
	 * Count the instances in the given InstanceMap for each event type
	 * then return an eventtype count map.
	 * @param instanceMap - the EventType -> Set<Instance> map
	 * @return a Map consisting of EventType->(number of instances for the event type) kv pairs
	 */
	protected Map<EventType, Integer> countInstances(Map<EventType, Set<Instance>> instanceMap) {
		HashMap<EventType, Integer> countMap = new HashMap<>();
		for(Entry<EventType, Set<Instance>> kv : instanceMap.entrySet()){
			if(kv.getValue() != null){
				countMap.put(kv.getKey(), kv.getValue().size());
			}
		}
		return countMap;
	}
	
//	public static void main(String[] args){
//		Map<EventType, Collection<Instance>> instanceMap = new HashMap<>();
//		
//		instanceMap.put(new EventType("AR"), null);
//		instanceMap.put(new EventType("CH"), null);
//		instanceMap.put(new EventType("FL"), null);
//		instanceMap.put(new EventType("EF"), null);
//		System.out.println(instanceMap);
//		new StcopMiner(instanceMap);
//	}
//	
}
