package edu.gsu.dmlab.solgrind.algo.stcopminers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ThreadLocalRandom;

import edu.gsu.dmlab.solgrind.algo.measures.significance.JaccardStar;
import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.operations.STOperations;
import edu.gsu.dmlab.solgrind.base.types.event.EventCooccurrence;
import edu.gsu.dmlab.solgrind.base.types.instance.InstanceCooccurrence;
import edu.gsu.dmlab.solgrind.base.types.instance.InstanceData;
import edu.gsu.dmlab.solgrind.index.CooccurrenceGraph;
import edu.gsu.dmlab.solgrind.index.GraphIndex;
import edu.gsu.dmlab.solgrind.index.InstanceVertex;
import edu.gsu.dmlab.solgrind.index.RelationEdge;
import net.sf.javaml.clustering.mcl.*;

/**
 * 
 * @author berkay
 *
 */

public class S3copMiner{

	static final double CCEth = 0.0001;
	static final double PIth = 0.0;
	
	static Map<EventCooccurrence, ArrayList<InstanceCooccurrence>> instanceCooccurrenceMap = null;
	static Map<EventType, Integer> instanceCountMap = null;
	static Map<EventType, Map<String, Instance>> instanceMap = null;
	static GraphIndex solgrind = null;
	static final float SAMPLING_FACTOR = 0.2F;
	private static final double SIGNIFICANCE_LEVEL = 0.1;
	
	public S3copMiner( Map<EventType, Set<Instance>> instanceSetMap){
		super();
		S3copMiner.solgrind = new GraphIndex(instanceSetMap);
		solgrind.buildCooccurrenceGraph();
		
		S3copMiner.instanceMap = transformMap(instanceSetMap);
		instanceCooccurrenceMap = new HashMap<EventCooccurrence, ArrayList<InstanceCooccurrence>>();
		instanceCountMap = countInstances(instanceSetMap);
		
		
		
		
		Set<EventType> eventTypes = instanceSetMap.keySet();
		
		
		ArrayList<EventCooccurrence> S2_CP = generateSize2CandidateSTCOPs(eventTypes);
		ArrayList<EventCooccurrence> S2_PP = discoverSize2STCOPs( S2_CP, instanceSetMap );
		double piHat = findSize2EstimatedP_solgrind(S2_CP, solgrind.getSolgrindCooccurrence());
		System.out.println("My pi_hat : " + piHat);
		
		ArrayList<EventCooccurrence> S2_SP = discoverSize2S3COPS(S2_PP, solgrind, piHat);
		System.out.println(S2_SP);
		
//		ArrayList<EventCooccurrence> prevPatterns = S2_PP;
//		//System.out.println("Size 2 Patterns==>>\n" + S2_PP);
//		while(prevPatterns != null && !prevPatterns.isEmpty()){
//			
//			ArrayList<EventCooccurrence> SK_CP = super.generateSizeKCandidateSTCOPs(prevPatterns);
//			System.out.println("Size-K Candidates: " + SK_CP);
//			ArrayList<EventCooccurrence> SK_PP = super.discoverSizeKSTCOPs( SK_CP, instanceCooccurrenceMap);
//			prevPatterns = SK_PP;
//		}
	}

	
	private ArrayList<EventCooccurrence> discoverSize2S3COPS(ArrayList<EventCooccurrence> s2_PP, GraphIndex solgrind2, double pHat) {
		ArrayList<EventCooccurrence> s2_SP = new ArrayList<EventCooccurrence>();
		CooccurrenceGraph sampledCOG = solgrind.generateNullCooccurrenceModel(pHat);
		System.out.println("Sampled graph has " + sampledCOG.edgeSet().size() + " edges...");

		int repetitions = 100;
		
		int[] nullModelRejectionCounts = new int[s2_PP.size()];
		for(int i = 0; i<nullModelRejectionCounts.length; i++){
			nullModelRejectionCounts[i]=0;
		}

		for (int i = 0; i < repetitions; i++){
			System.out.println("Repetition:" + (i+1));
			System.out.println("will check for " + s2_PP);
			for (int j = 0; j < s2_PP.size(); j++){
				EventCooccurrence pec = s2_PP.get(j);
				double pi_null = calculateNullModelPi(pec, sampledCOG);
				System.out.println("For " + pec + "-- pi_null: " + pi_null);
				if(pi_null < pec.getPrevalence()){
					System.out.println("\t pi_null < pi_obs for " + pec);
					nullModelRejectionCounts[j]++;
				}
			}
		}
		
		for (int i = 0; i< nullModelRejectionCounts.length; i++){
			// csec is candidate significant event co-occurrence
			EventCooccurrence csec = s2_PP.get(i);
			double p = (double) nullModelRejectionCounts[i] / (double)repetitions;
			if(p > 1 - SIGNIFICANCE_LEVEL){
				s2_SP.add(csec);
				System.out.println(csec + " passed p-test " + p);
			} else{
				System.out.println(csec + " could not pass p-test " + p);
			}
		}
		
		return s2_SP;
	}



	private double calculateNullModelPi(EventCooccurrence pec, CooccurrenceGraph sampledCOG) {
//		instanceCountMap.get(null)
		HashMap<EventType, Set<String> > instancesPerEventType = new HashMap<EventType, Set<String>>();
		for(EventType et : pec.getEventTypes()){
			instancesPerEventType.put(et, new HashSet<String>() );
		}
		
		Iterator<RelationEdge> it = sampledCOG.edgeSet().iterator();
		while(it.hasNext()){
			RelationEdge coEdge = it.next();
			InstanceVertex v1 = sampledCOG.getEdgeSource(coEdge);
			InstanceVertex v2 = sampledCOG.getEdgeTarget(coEdge);
//			System.out.println(v1 +""+ coEdge + v2);
			ArrayList<InstanceVertex> instanceVertices = new ArrayList<InstanceVertex>();
			instanceVertices.add(v1);
			instanceVertices.add(v2);
			EventCooccurrence ec = getEventCooccurrenceOf(instanceVertices);
			if(ec.equals(pec)){
				for(InstanceVertex iv : instanceVertices){
					instancesPerEventType.get(iv.getType()).add(iv.getId());
				}
			}
		}
		double pi = Double.MAX_VALUE;
		for(Entry<EventType, Set<String>> etParticipatingSet : instancesPerEventType.entrySet()){
			EventType et = etParticipatingSet.getKey();
			Set<String> participatingSet = etParticipatingSet.getValue();
			int etCount = participatingSet.size();
			double pr =  (double)etCount / (double)instanceCountMap.get(et);
			if(pr < pi){
				pi = pr;
			}
		}
		
		return pi;
	}


	private EventCooccurrence getEventCooccurrenceOf(ArrayList<InstanceVertex> instanceVertices) {
		ArrayList<EventType> eventTypes = new ArrayList<>();
		for(InstanceVertex v : instanceVertices){
			eventTypes.add(v.getType());
		}
		return new EventCooccurrence(eventTypes );
	}


	private double findSize2EstimatedP_solgrind(ArrayList<EventCooccurrence> s2_CP, CooccurrenceGraph solgrindCo) {
		int totalCoCount = solgrindCo.edgeSet().size();
		
//		solgrind.findKCliques(2);

		System.out.println("Total count of co-occurrences: " + totalCoCount);

		int possibleCoCount = 0;
		for(EventCooccurrence size2Candidate : s2_CP){
			ArrayList<EventType> eventTypes = new ArrayList<EventType>(size2Candidate.getEventTypes());
			
			possibleCoCount += (instanceCountMap.get(eventTypes.get(0)) * instanceCountMap.get(eventTypes.get(1)));
		}
		System.out.println("Total count of possible co-occurrences: " + possibleCoCount);

		
		double p_hat = new Double(totalCoCount) / new Double(possibleCoCount);
		return p_hat;
		
	}
	
	
	
	

	private double findSize2EstimatedP(
			ArrayList<EventCooccurrence> s2_CP, 
			Map<EventType, Set<Instance>> instanceMap) {
		
		int totalCoCount = 0;
		int possibleCoCount = 0;
		for(EventCooccurrence size2Candidate : s2_CP){
			
			ArrayList<EventType> eventTypes = new ArrayList<EventType>(size2Candidate.getEventTypes());
			
			
			Set<Instance> e1Instances = instanceMap.get(eventTypes.get(0));
			Set<Instance> e2Instances = instanceMap.get(eventTypes.get(1));
//			System.out.println("Finding co-occurrences for " + eventTypes.get(0) + ", " + eventTypes.get(1));
			
			ArrayList<InstanceCooccurrence> significantCOs = findAllCooccurences(e1Instances, e2Instances);
			totalCoCount += significantCOs.size();
			
			possibleCoCount += (instanceCountMap.get(eventTypes.get(0)) * instanceCountMap.get(eventTypes.get(1)));
		}
		System.out.println("Total count of co-occurrences: " + totalCoCount);
		System.out.println("Total count of possible co-occurrences: " + possibleCoCount);
		
		double p_hat = new Double(totalCoCount) / new Double(possibleCoCount);
		return p_hat;
	}
	

	/**
	 * This method discovers size-2 STCOPs from given size-2 candidate patterns
	 * and the provided instance map. The event types in instance map and the participating
	 * event types in the candidate pattern list must match.
	 * @param s2_CP -- size-2 candidadate patterns 
	 * @param instanceMap -- instance map for each event type.
	 * @return - an array list of STCOPs (in the form of EventCooccurrence objects).
	 */
	protected ArrayList<EventCooccurrence> discoverSize2STCOPs(ArrayList<EventCooccurrence> s2_CP,
			Map<EventType, Set<Instance>> instanceMap) {
		ArrayList<EventCooccurrence> s2_pp = new ArrayList<>();
		for(EventCooccurrence size2Candidate : s2_CP){
			
			ArrayList<EventType> eventTypes = new ArrayList<EventType>(size2Candidate.getEventTypes());
			
			
			Set<Instance> e1Instances = instanceMap.get(eventTypes.get(0));
			Set<Instance> e2Instances = instanceMap.get(eventTypes.get(1));
			
			ArrayList<InstanceCooccurrence> significantCOs = findCooccurences(e1Instances, e2Instances);
			double pi = calculatePi(size2Candidate, significantCOs);
			if(pi > S3copMiner.PIth){
				System.out.println("Candidate passed the pi threshold " + size2Candidate + " PI:" + pi);
				size2Candidate.setPrevalence(pi);
				instanceCooccurrenceMap.put(size2Candidate, significantCOs);
				s2_pp.add(size2Candidate);
			}
		}
		
		return s2_pp;
	}

	protected double calculatePi(EventCooccurrence candidate, ArrayList<InstanceCooccurrence> cooccurrences) {
		HashMap<EventType, Set<String>> participatingInstanceMap = new HashMap<>();
		for(EventType et: candidate.getEventTypes()){
			participatingInstanceMap.put(et, new TreeSet<>());
		}
		
		for(InstanceCooccurrence cooccurrence : cooccurrences){
			for(EventType et : candidate.getEventTypes()){
				String id = cooccurrence.getInstanceIdOfType(et);
				participatingInstanceMap.get(et).add(id);
			}
		}
		double pi = Double.MAX_VALUE;
		for(Entry<EventType, Set<String>> kv : participatingInstanceMap.entrySet()){
			double total = instanceCountMap.get(kv.getKey());
			double participatorCount = participatingInstanceMap.get(kv.getKey()).size();
			double pr = participatorCount/total; // participation ratio
			if(pr < pi){
				pi = pr;
			}
		}
		if(pi == Double.MAX_VALUE){
			System.out.println("Warning invalid pi for" + candidate + " returning pi=-1.0");
			return -1.0;
		} else{
			return pi;
		}
	}

	/**
	 * This generates size-2 candidate patterns
	 * @param eventTypes: set of event types
	 * @return an arraylist of candidates to be examined
	 */
	protected ArrayList<EventCooccurrence> generateSize2CandidateSTCOPs(Set<EventType> eventTypes) {
		ArrayList<EventCooccurrence> size2Candidates = new ArrayList<>();
		ArrayList<EventType> etList = new ArrayList<>(eventTypes);
		for(int i = 0; i < etList.size(); i++){
			for(int j = i+1; j < etList.size(); j++){
				EventCooccurrence candidate = new EventCooccurrence();
				candidate.addEventType(etList.get(i));
				candidate.addEventType(etList.get(j));
				size2Candidates.add(candidate);
			}
		}
		System.out.println("Size-2 Candidates:");
		for(EventCooccurrence candidate : size2Candidates){
			System.out.println(candidate);
		}
		return size2Candidates;
	}

	/**
	 * Find all ST co-occurrences between two sets of instances without cce_th
	 * @param e1Instances - instances of event type e1
	 * @param e2Instances - instances of event type e2
	 * @return An ArrayList of pattern instances (in the form of InstanceCooccurrence objects)
	 */
	protected ArrayList<InstanceCooccurrence> findAllCooccurences(Set<Instance> e1Instances, 
			Set<Instance> e2Instances) {
		
		ArrayList<InstanceCooccurrence> overlappingCooccurrences = new ArrayList<>();
		for(Instance e1i: e1Instances) {
			for(Instance e2i: e2Instances) {
				InstanceCooccurrence ic = findCooccurrence(e1i, e2i);
				if(ic.isCceValid()){
					//System.out.println("Overlapping ST CO: " + ic );
					overlappingCooccurrences.add(ic);
				}
			}
		}
		return overlappingCooccurrences;
	}
	

	
	protected Map<EventType, Map<String, Instance>> transformMap(Map<EventType, Set<Instance>> instanceSetMap) {
		HashMap<EventType, Map<String, Instance>> instanceMap = new HashMap<>();
		for(EventType et : instanceSetMap.keySet()){
			
			instanceMap.put(et, new HashMap<String, Instance>());
//			System.out.println("Adding " + et);
//			System.out.println("There are " + instanceSetMap.get(et).size() + " instances originally");
			
			for(Instance ins : instanceSetMap.get(et)){
				instanceMap.get(et).put( ins.getId(), ins);
			}
//			System.out.println("I added " + instanceMap.get(et).size() + " in the end" );
		}
		return instanceMap;
	}
	
	/**
	 * Count the instances in the given InstanceMap for each event type
	 * then return an EventType count map.
	 * @param instanceMap - the EventType -> Set<Instance> map
	 * @return a Map consisting of EventType->(number of instances for the event type) kv pairs
	 */
	protected Map<EventType, Integer> countInstances(Map<EventType, Set<Instance>> instanceMap) {
		HashMap<EventType, Integer> countMap = new HashMap<>();
		for(Entry<EventType, Set<Instance>> kv : instanceMap.entrySet()){
			if(kv.getValue() != null){
				countMap.put(kv.getKey(), kv.getValue().size());
			}
		}
		return countMap;
	}
	
	/**
	 * Find ST co-occurrences between two sets of instances
	 * @param e1Instances - instances of event type e1
	 * @param e2Instances - instances of event type e2
	 * @return An ArrayList of pattern instances (in the form of InstanceCooccurrence objects)
	 */
	protected ArrayList<InstanceCooccurrence> findCooccurences(Set<Instance> e1Instances, 
			Set<Instance> e2Instances) {

		ArrayList<InstanceCooccurrence> overlappingCooccurrences = new ArrayList<>();
		for(Instance e1i: e1Instances) {
			for(Instance e2i: e2Instances) {
				InstanceCooccurrence ic = findCooccurrence(e1i, e2i);
				if(ic.isCceValid()){
					//System.out.println("Overlapping ST CO: " + ic );
					overlappingCooccurrences.add(ic);
				}
			}
		}
		return overlappingCooccurrences;
	}
	
	
	/**
	 * Given two instances ins1 and ins2, this method creates an InstanceCoocurrence
	 * object (which may or may not be empty). The empty object corresponds to no pattern
	 * instance, and it is invalid. The non-empty ones are calculated.
	 * USE THIS WITH CAUTION, THIS IS AN EXPENSIVE OPERATION
	 * 
	 * @param ins1 - instance 1 to be checked
	 * @param ins2 - instance 2 to be checked
	 * @return a pattern instance (or an empty co-occurrence)
	 */
	protected InstanceCooccurrence findCooccurrence(Instance ins1, Instance ins2) {
		InstanceCooccurrence ic = new InstanceCooccurrence();
		if(!STOperations.stIntersects(ins1.getTrajectory(), ins2.getTrajectory())){
			return ic; //return empty ic
		} else{
			double jStar = new JaccardStar().calculate(ins1, ins2);
			if(jStar <= S3copMiner.CCEth){
				return ic; // return empty ic
			} else {
				ic.append(new InstanceData(ins1));
				ic.append(new InstanceData(ins2));
				ic.setCce(jStar);
				ic.setcceType("J*");
				return ic;
			}
		}

	
	}


//	private ArrayList<Instance> flattenInstanceMap(){
//		ArrayList<Instance> instanceList = new ArrayList<Instance>();
//		for(Map<String, Instance> eventMap : S3copMiner.instanceMap.values() ){
//			for( Instance ins : eventMap.values() ){
//				instanceList.add(ins);
//			}
//		}
//		return instanceList;
//	}
//	
//	private Map<EventType, Map<String, Instance>> sampleInstances(ArrayList<Instance> instanceList, float samplingFactor){
//		
//		Integer[] arr = new Integer[instanceList.size()];
//		Arrays.fill(arr, 0, Math.round(instanceList.size()*samplingFactor), 1);
//		Arrays.fill(arr, Math.round(instanceList.size()*samplingFactor), instanceList.size(), 0);
//		List<Integer> list = Arrays.asList(arr);
//		Collections.shuffle(list); //list is now in random order
////		System.out.println(list);
//		
//		
//		HashMap<EventType, Map<String, Instance>> sampledInstanceMap = new HashMap<EventType, Map<String, Instance>>();
//		int i = -1;
//		for(Instance ins : instanceList){
//			i++;
//			if(list.get(i) == 0){ 
//				continue; 
//			}
//			
//			if( !sampledInstanceMap.containsKey(ins.getType()) ){
//				HashMap<String, Instance> tempInstanceMap = new HashMap<String, Instance>();
//				tempInstanceMap.put(ins.getId(), ins);
//				sampledInstanceMap.put(ins.getType(), tempInstanceMap);
//			} else{
//				sampledInstanceMap.get(ins.getType()).put(ins.getId(), ins);
//			}
//
//		}
//		
//		return sampledInstanceMap;
//	}
	
}
