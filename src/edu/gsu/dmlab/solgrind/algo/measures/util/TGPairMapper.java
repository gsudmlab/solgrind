package edu.gsu.dmlab.solgrind.algo.measures.util;

import java.util.Map;
import java.util.TreeMap;

import com.vividsolutions.jts.geom.Geometry;

import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.types.essential.TGPair;

public class TGPairMapper {
	
	public static Map<Long, Geometry> map(Instance ins){
		
		Map<Long, Geometry> tgpMap = new TreeMap<Long, Geometry>();
		for( TGPair tgp : ins.getTrajectory().getTGPairs() ){
			
			tgpMap.put(tgp.getTInterval().getStartTime(), tgp.getGeometry());
			
		}
		return tgpMap;
	}
	
	public static double volume(Instance i) {

        double v = 0;
        for(Geometry g: TGPairMapper.map(i).values()){
            v += g.getArea();
        }
        return v;
    }

}
