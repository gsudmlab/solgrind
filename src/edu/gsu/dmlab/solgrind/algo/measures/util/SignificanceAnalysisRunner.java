package edu.gsu.dmlab.solgrind.algo.measures.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.collect.Sets;
import com.vividsolutions.jts.index.strtree.SIRtree;
import com.vividsolutions.jts.index.strtree.STRtree;

import edu.gsu.dmlab.solgrind.algo.measures.implementation.Jaccard;
import edu.gsu.dmlab.solgrind.algo.measures.implementation.JaccardPlus;
import edu.gsu.dmlab.solgrind.algo.measures.implementation.JaccardStar;
import edu.gsu.dmlab.solgrind.algo.measures.implementation.OMAX;
import edu.gsu.dmlab.solgrind.algo.measures.implementation.OMIN;
import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.types.essential.TGPair;
import edu.gsu.dmlab.solgrind.base.types.essential.TInterval;
import edu.gsu.dmlab.solgrind.fileio.CSVDataParser;
import javafx.util.Pair;

public class SignificanceAnalysisRunner {
    //private static DataManager dataManager;
    public static String RUNTIME_BASE;
    public static String ANALYSIS_BASE;
    public static String BASE_PATH;
    public static String DATA_DIR;
    public static int MONTH_INDEX;

    public static long ONE_MINUTE = 1000*60; //in milliseconds

    public static final int numberOfTest = 3;

    public static Map<EventType, Set<Instance>> instanceMap = null;
    public static Map<EventType, SIRtree> instanceIndex = null;
    
    static String [] arg = new String[]{"results", "/Users/ahmetkucuk/Documents/Research/Berkay/interpolated_2012_v2", "9"};

    /**
     * Sample Run:
     * nohup java -cp "target/lib-0.0.1-jar-with-dependencies.jar" edu.gsu.dmlab.solgrind.algo.measures.opt.SignificanceAnalysisRunner /home/ahmet/workspace/jaccard-star-experiments/results/ /home/ahmet/workspace/jaccard-star-experiments/data/interpolated_2012_v2/ 0 >  /home/ahmet/workspace/jaccard-star-experiments/output.txt 2>&1 &
     *
     */
    public static void main(String[] args) throws Exception {

        if(args.length < 3) {
            System.out.println("Two arguments needed: Output directory, Input Directory, month");
        }
        BASE_PATH = args[0];
        DATA_DIR = args[1];
        MONTH_INDEX = Integer.parseInt(args[2]);
//        BASE_PATH = "/Users/ahmetkucuk/Documents/Research/Berkay/results";
//        DATA_DIR = "/Users/ahmetkucuk/Documents/Research/Berkay/";

        
//        String[] eventTypeArray = {"ar","ch"};
        String[] eventTypeArray = {"ar", "ch", "ss", "fi", "fl", "sg", "ef"};

        TInterval[] tiArray = new TInterval[] {
        		new TInterval("2012-01-01 00:00:00", "2012-03-31 23:59:59"),
        		new TInterval("2012-04-01 00:00:00", "2012-06-30 23:59:59"),
                new TInterval("2012-07-01 00:00:00", "2012-09-30 23:59:59"),
                new TInterval("2012-10-01 00:00:00", "2012-12-31 23:59:59")
                };

//        TInterval[] tiArray = new TInterval[] {
//                new TInterval("2012-01-01 00:00:00", "2012-01-15 23:59:59"),
//                new TInterval("2012-01-16 00:00:00", "2012-01-31 23:59:59"),
//
//                new TInterval("2012-02-01 00:00:00", "2012-02-15 23:59:59"),
//                new TInterval("2012-02-16 00:00:00", "2012-02-28 23:59:59"),
//
//                new TInterval("2012-03-01 00:00:00", "2012-03-15 23:59:59"),
//                new TInterval("2012-03-16 00:00:00", "2012-03-31 23:59:59"),
//
//                new TInterval("2012-04-01 00:00:00", "2012-04-15 23:59:59"),
//                new TInterval("2012-04-16 00:00:00", "2012-04-30 23:59:59"),
//
//                new TInterval("2012-05-01 00:00:00", "2012-05-15 23:59:59"),
//                new TInterval("2012-05-16 00:00:00", "2012-05-31 23:59:59"),
//
//                new TInterval("2012-06-01 00:00:00", "2012-06-15 23:59:59"),
//                new TInterval("2012-06-16 00:00:00", "2012-06-30 23:59:59"),
//
//                new TInterval("2012-07-01 00:00:00", "2012-07-15 23:59:59"),
//                new TInterval("2012-07-16 00:00:00", "2012-07-31 23:59:59"),
//
//                new TInterval("2012-08-01 00:00:00", "2012-08-15 23:59:59"),
//                new TInterval("2012-08-16 00:00:00", "2012-08-31 23:59:59"),
//
//                new TInterval("2012-09-01 00:00:00", "2012-09-15 23:59:59"),
//                new TInterval("2012-09-16 00:00:00", "2012-09-30 23:59:59"),
//
//                new TInterval("2012-10-01 00:00:00", "2012-10-15 23:59:59"),
//                new TInterval("2012-10-16 00:00:00", "2012-10-31 23:59:59"),
//
//                new TInterval("2012-11-01 00:00:00", "2012-11-15 23:59:59"),
//                new TInterval("2012-11-16 00:00:00", "2012-11-30 23:59:59"),
//
//                new TInterval("2012-12-01 00:00:00", "2012-12-15 23:59:59"),
//                new TInterval("2012-12-16 00:00:00", "2012-12-31 23:59:59")
//        };
//        TInterval[] tiArray = new TInterval[] {new TInterval(0, Long.MAX_VALUE)};
//        String[] eventTypeArray = {"fl", "sg", "ef"};
        ArrayList<EventType> eventTypes = createEventTypes( eventTypeArray);

        int quarter = 0;
//        for(TInterval ti: tiArray) {
        TInterval ti = tiArray[MONTH_INDEX];
        RUNTIME_BASE =  BASE_PATH + "/JaccardRuntime/";
        ANALYSIS_BASE = BASE_PATH + "/JaccardMap/";
        instanceMap = new CSVDataParser(DATA_DIR).readToInstanceMap(eventTypes, ti);
        calculateAndPrintStatistics(MONTH_INDEX);

//        createInvertedIndex();

//        runAnalysis();
//        System.gc();
//        }
        System.out.println("Finished");
    }

	private static void createInvertedIndex() {
		instanceIndex = new ConcurrentHashMap<>(11);
		for(Entry<EventType, Set<Instance>> kv: instanceMap.entrySet()){
			EventType et = kv.getKey();
			instanceIndex.put(et, new SIRtree());
			for(Instance ins : kv.getValue()){
				long startTime = ins.getStartTime();
				long endTime = ins.getEndTime();
				instanceIndex.get(et).insert(startTime, endTime, ins);
			}
		}
		
	}

	private static ArrayList<EventType> createEventTypes(String[] eventTypeArray) {
		ArrayList<EventType> eventTypes = new ArrayList<>();
		for(String etStr : eventTypeArray){
        	eventTypes.add(new EventType(etStr));
        }
		return eventTypes;
	}

    public static void runAnalysis() throws Exception {

    	Set<Set<EventType>> powerSet = Sets.powerSet(instanceMap.keySet());
    	
        //Set<Set<String>> powerSet = Sets.powerSet(dataManager.getInstanceByTypeMap().keySet());
        Set<Set<EventType>> twoSets = Sets.filter(powerSet, types -> types.size() == 2);
        Set<Set<EventType>> threeSets = Sets.filter(powerSet, types -> types.size() == 3);

        File file = new File(RUNTIME_BASE + "THREE/");
        file.mkdirs();

        file = new File(RUNTIME_BASE + "TWO/");
        file.mkdirs();

        file = new File(ANALYSIS_BASE + "THREE/");
        file.mkdirs();

        file = new File(ANALYSIS_BASE + "TWO/");
        file.mkdirs();

        long t1 = System.currentTimeMillis();

        for (Set<EventType> set : twoSets) {
            List<EventType> features = new ArrayList<>(set);
            EventType feature1 = features.get(0);
            EventType feature2 = features.get(1);
            calculateForTwo(feature1, feature2);
        }

        System.out.println("Two finished: " + ((System.currentTimeMillis() - t1)/1000));
        t1 = System.currentTimeMillis();
/*
        for (Set<EventType> set : threeSets) {
            long t2 = System.currentTimeMillis();
            List<EventType> features = new ArrayList<>(set);
            EventType feature1 = features.get(0);
            EventType feature2 = features.get(1);
            EventType feature3 = features.get(2);
            _indexed_CalculateForThree(feature1, feature2, feature3);

            System.out.println("Three finished for: " + set + " " + ((System.currentTimeMillis() - t2)/1000));
        }

        System.out.println("Three finished: " + ((System.currentTimeMillis() - t1)/1000));
*/
    }

    @SuppressWarnings("unchecked")
	public static void calculateForTwo(EventType feature1, EventType feature2) throws Exception {
        long totalJaccardPlusTime = 0;
        long totalJaccardStarTime = 0;
        long totalJaccardTime = 0;
        long totalOmaxTime = 0;
        long totalOminTime = 0;

        SortedMap<String, Pair<Double, Long>> resultMap = new TreeMap<>();
        Set<Instance> e1Instances = instanceMap.get(feature1);
        //Set<Instance> e2Instances = instanceMap.get(feature2);
        for (Instance i1 : e1Instances) {
        	
        	@SuppressWarnings("unchecked")
			List<Instance> e2Instances = new ArrayList<>();;
			try {
				SIRtree idx2 = instanceIndex.get(feature2);
				e2Instances = idx2.query(i1.getStartTime(), i1.getEndTime());
			} catch (Exception e1) {
				System.out.println("EventType:" + feature1 + "\n"
						  +"Overlap Time Interval: " + i1.getInterval() + "\n"
						  +"Instance 1 : " + i1.getId() );
				e1.printStackTrace();
				continue;
			}
        	
            for (Instance i2 : e2Instances) {
                if(!i1.getInterval().overlaps(i2.getInterval())) continue;
                long t1, t2;
                double jaccard = 0;
                long jaccardTime = 0;
                for(int i = 0; i < numberOfTest; i++) {
                    t1 = System.nanoTime();
                    jaccard = Jaccard.jaccard(i1, i2);
                    t2 = System.nanoTime();

                    jaccardTime += t2 - t1;
                }
                jaccardTime = jaccardTime/numberOfTest;
                resultMap.put("Jaccard", new Pair<>(jaccard, jaccardTime));

                double jaccardPlus = 0;
                long jaccardPlusTime = 0;
                for(int i = 0; i < numberOfTest; i++) {

                    t1 = System.nanoTime();
                    jaccardPlus = JaccardPlus.jaccardPlus2(i1, i2);
                    t2 = System.nanoTime();

                    jaccardPlusTime += t2 - t1;
                }
                jaccardPlusTime = jaccardPlusTime/numberOfTest;
                resultMap.put("JaccardPlus", new Pair<>(jaccardPlus, jaccardPlusTime));

                double jaccardStar = 0;
                long jaccardStarTime = 0;
                for(int i = 0; i < numberOfTest; i++) {

                    t1 = System.nanoTime();
                    jaccardStar = JaccardStar.jaccardStar2(i1, i2);
                    t2 = System.nanoTime();

                    jaccardStarTime += t2-t1;
                }
                jaccardStarTime = jaccardStarTime/numberOfTest;
                resultMap.put("JaccardStar", new Pair<>(jaccardStar, jaccardStarTime));


                double omax = 0;
                long omaxTime = 0;
                for(int i = 0; i < numberOfTest; i++) {

                    t1 = System.nanoTime();
                    omax = OMAX.omax(i1, i2);
                    t2 = System.nanoTime();

                    omaxTime += t2 - t1;
                }

                omaxTime = omaxTime/numberOfTest;
                resultMap.put("OMAX", new Pair<>(omax, omaxTime));

                double omin = 0;
                long ominTime = 0;
                for(int i = 0; i < numberOfTest; i++) {

                    t1 = System.nanoTime();
                    omin = OMIN.omin(i1, i2);
                    t2 = System.nanoTime();

                    ominTime += t2 - t1;
                }
                ominTime = ominTime/numberOfTest;
                resultMap.put("OMIN", new Pair<>(omin, ominTime));


                if(jaccard > 0) {
                    totalJaccardPlusTime += jaccardPlusTime;
                    totalJaccardTime += jaccardTime;
                    totalJaccardStarTime += jaccardStarTime;
                    totalOmaxTime += omaxTime;
                    totalOminTime += ominTime;
                    SignificanceAnalysis.writeAnalysisForTwo(i1, i2, resultMap);
                }
            }
        }
        printTimeToAFile(RUNTIME_BASE + "TWO/" + feature1.getType().substring(0, 2) 
        		+ "_" + feature2.getType().substring(0, 2) + ".csv", totalJaccardPlusTime, totalJaccardStarTime, totalJaccardTime, totalOmaxTime, totalOminTime);
        System.out.println("Jaccard: " + totalJaccardTime/1000 + " Jaccard Star: " +  totalJaccardStarTime/1000 + " Jaccard Plus: " +  totalJaccardPlusTime/1000 + " Omax: " + totalOmaxTime/1000 + " OMin: " + totalOminTime/1000);
    }

    public static void calculateForThree(EventType feature1, EventType feature2, EventType feature3) throws Exception {
        long totalJaccardPlusTime = 0;
        long totalJaccardStarTime = 0;
        long totalJaccardTime = 0;
        long totalOmaxTime = 0;
        long totalOminTime = 0;

        long totalJaccard = 0;
        long totalJaccardStar = 0;
        SortedMap<String, Pair<Double, Long>> resultMap = new TreeMap<>();

        int counter = 0;
        Set<Instance> e1Instances = instanceMap.get(feature1);
        Set<Instance> e2Instances = instanceMap.get(feature2);
        Set<Instance> e3Instances = instanceMap.get(feature3);
        for (Instance i1 : e1Instances) {
            for (Instance i2 : e2Instances) {
                if(!i1.getInterval().overlaps(i2.getInterval())) continue;
                for (Instance i3 : e3Instances) {
                    if(!i2.getInterval().overlaps(i3.getInterval())) continue;
                    TInterval oti = i1.getInterval().intersection(i2.getInterval());
                    if(!oti.overlaps(i3.getInterval())) continue;

                    List<Instance> instances = Arrays.asList(i1, i2, i3);
                    long t1, t2;

                    double jaccard = 0;
                    long jaccardTime = 0;
                    for(int i = 0; i < numberOfTest; i++) {
                        t1 = System.nanoTime();
                        jaccard = Jaccard.jaccard(instances);
                        t2 = System.nanoTime();

                        jaccardTime += t2 - t1;
                    }
                    jaccardTime = jaccardTime/numberOfTest;
                    resultMap.put("Jaccard", new Pair<>(jaccard, jaccardTime));

                    double jaccardPlus = 0;
                    long jaccardPlusTime = 0;
                    for(int i = 0; i < numberOfTest; i++) {

                        t1 = System.nanoTime();
                        jaccardPlus = JaccardPlus.jaccardPlus(instances);
                        t2 = System.nanoTime();

                        jaccardPlusTime += t2 - t1;
                    }
                    jaccardPlusTime = jaccardPlusTime/numberOfTest;
                    resultMap.put("JaccardPlus", new Pair<>(jaccardPlus, jaccardPlusTime));

                    double jaccardStar = 0;
                    long jaccardStarTime = 0;
                    for(int i = 0; i < numberOfTest; i++) {

                        t1 = System.nanoTime();
                        jaccardStar = JaccardStar.jaccardStar(instances);
                        t2 = System.nanoTime();

                        jaccardStarTime += t2-t1;
                    }
                    jaccardStarTime = jaccardStarTime/numberOfTest;
                    resultMap.put("JaccardStar", new Pair<>(jaccardStar, jaccardStarTime));


                    double omax = 0;
                    long omaxTime = 0;
                    for(int i = 0; i < numberOfTest; i++) {

                        t1 = System.nanoTime();
                        omax = OMAX.omax(instances);
                        t2 = System.nanoTime();

                        omaxTime += t2 - t1;
                    }

                    omaxTime = omaxTime/numberOfTest;
                    resultMap.put("OMAX", new Pair<>(omax, omaxTime));

                    double omin = 0;
                    long ominTime = 0;
                    for(int i = 0; i < numberOfTest; i++) {

                        t1 = System.nanoTime();
                        omin = OMIN.omin(instances);
                        t2 = System.nanoTime();

                        ominTime += t2 - t1;
                    }
                    ominTime = ominTime/numberOfTest;
                    resultMap.put("OMIN", new Pair<>(omin, ominTime));

                    if (jaccard > 0) {
                        totalJaccardPlusTime += jaccardPlusTime;
                        totalJaccardTime += jaccardTime;
                        totalJaccardStarTime += jaccardStarTime;
                        totalOmaxTime += omaxTime;
                        totalOminTime += ominTime;
                        SignificanceAnalysis.writeAnalysisForThree(i1, i2, i3, resultMap);
                    }
                    counter++;
//                    if(counter % 1000 == 0) {
//                        System.out.println("Continue: " + counter + " " + feature1 + "_" + feature2 + "_" + feature3);
//                    }
                }
            }
        }

        printTimeToAFile(RUNTIME_BASE + "THREE/" + feature1.getType().substring(0, 2)
        		+ "_" + feature2.getType().substring(0, 2)
        		+ "_" + feature3.getType().substring(0, 2) + ".csv", totalJaccardPlusTime, totalJaccardStarTime, totalJaccardTime, totalOmaxTime, totalOminTime);
        System.out.println("jaccard: " + ((totalJaccard)) + " jaccard Star: " + ((totalJaccardStar)) );
    }

    
    
    public static void printTimeToAFile(String fileName, long jaccardPlusTime, long jaccardStarTime, long jaccardTime, long omaxTime, long ominTime) throws Exception {
        PrintWriter writer = new PrintWriter(fileName, "UTF-8");
        writer.println("JaccardPlus," + jaccardPlusTime);
        writer.println("JaccardStar," + jaccardStarTime);
        writer.println("Jaccard," + jaccardTime);
        writer.println("OMAX," + omaxTime);
        writer.println("OMIN," + ominTime);
        writer.close();
    }
    
    @SuppressWarnings("unchecked")
	public static void _indexed_CalculateForThree(EventType feature1, EventType feature2, EventType feature3) throws Exception {
        long totalJaccardPlusTime = 0;
        long totalJaccardStarTime = 0;
        long totalJaccardTime = 0;
        long totalOmaxTime = 0;
        long totalOminTime = 0;

        long totalJaccard = 0;
        long totalJaccardStar = 0;
        SortedMap<String, Pair<Double, Long>> resultMap = new TreeMap<>();

        int counter = 0;
        Set<Instance> e1Instances = instanceMap.get(feature1);
        for (Instance i1 : e1Instances) {
        	
        	@SuppressWarnings("unchecked")
			List<Instance> e2Instances = new ArrayList<>();;
			try {
				SIRtree idx2 = instanceIndex.get(feature2);
				e2Instances = idx2.query(i1.getStartTime(), i1.getEndTime());
			} catch (Exception e1) {
				System.out.println("EventType:" + feature1 + "\n"
						  +"Overlap Time Interval: " + i1.getInterval() + "\n"
						  +"Instance 1 : " + i1.getId() );
				e1.printStackTrace();
				continue;
			}
        	
            for (Instance i2 : e2Instances) {
            	@SuppressWarnings("unchecked")
				List<Instance> e3Instances = new ArrayList<>();
            	TInterval overlapTimeInterval = new TInterval(0, 1);
				try {
					overlapTimeInterval = i1.getInterval().intersection(i2.getInterval());
					SIRtree idx3 = instanceIndex.get(feature3);
					e3Instances = idx3.query(overlapTimeInterval.getStartTime(), overlapTimeInterval.getEndTime());
				} catch (Exception e) {
					System.out.println("EventType:" + feature3 + "\n"
									  +"Overlap Time Interval: " + overlapTimeInterval + "\n"
									  +"Instance 1 and 2 : " + i1.getId() + "\t" + i2.getId() );
					e.printStackTrace();
					continue;
				}
            	
                //if(!i1.getInterval().overlaps(i2.getInterval())) continue;
                for (Instance i3 : e3Instances) {
                    //if(!i2.getInterval().overlaps(i3.getInterval())) continue;
                    //if(!overlapTimeInterval.overlaps(i3.getInterval())) continue;

                    List<Instance> instances = Arrays.asList(i1, i2, i3);
                    long t1, t2;

                    double jaccard = 0;
                    long jaccardTime = 0;
                    for(int i = 0; i < numberOfTest; i++) {
                        t1 = System.nanoTime();
                        jaccard = Jaccard.jaccard(instances);
                        if(jaccard <= 0.0){ break;}
                        t2 = System.nanoTime();

                        jaccardTime += t2 - t1;
                    }
                    if(jaccard <= 0.0){ continue;}
                    jaccardTime = jaccardTime/numberOfTest;
                    resultMap.put("Jaccard", new Pair<>(jaccard, jaccardTime));

                    double jaccardPlus = 0;
                    long jaccardPlusTime = 0;
                    for(int i = 0; i < numberOfTest; i++) {

                        t1 = System.nanoTime();
                        jaccardPlus = JaccardPlus.jaccardPlus(instances);
                        t2 = System.nanoTime();

                        jaccardPlusTime += t2 - t1;
                    }
                    jaccardPlusTime = jaccardPlusTime/numberOfTest;
                    resultMap.put("JaccardPlus", new Pair<>(jaccardPlus, jaccardPlusTime));

                    double jaccardStar = 0;
                    long jaccardStarTime = 0;
                    for(int i = 0; i < numberOfTest; i++) {

                        t1 = System.nanoTime();
                        jaccardStar = JaccardStar.jaccardStar(instances);
                        t2 = System.nanoTime();

                        jaccardStarTime += t2-t1;
                    }
                    jaccardStarTime = jaccardStarTime/numberOfTest;
                    resultMap.put("JaccardStar", new Pair<>(jaccardStar, jaccardStarTime));


                    double omax = 0;
                    long omaxTime = 0;
                    for(int i = 0; i < numberOfTest; i++) {

                        t1 = System.nanoTime();
                        omax = OMAX.omax(instances);
                        t2 = System.nanoTime();

                        omaxTime += t2 - t1;
                    }

                    omaxTime = omaxTime/numberOfTest;
                    resultMap.put("OMAX", new Pair<>(omax, omaxTime));

                    double omin = 0;
                    long ominTime = 0;
                    for(int i = 0; i < numberOfTest; i++) {

                        t1 = System.nanoTime();
                        omin = OMIN.omin(instances);
                        t2 = System.nanoTime();

                        ominTime += t2 - t1;
                    }
                    ominTime = ominTime/numberOfTest;
                    resultMap.put("OMIN", new Pair<>(omin, ominTime));

                    if (jaccard > 0) {
                        totalJaccardPlusTime += jaccardPlusTime;
                        totalJaccardTime += jaccardTime;
                        totalJaccardStarTime += jaccardStarTime;
                        totalOmaxTime += omaxTime;
                        totalOminTime += ominTime;
                        SignificanceAnalysis.writeAnalysisForThree(i1, i2, i3, resultMap);
                    }
                    counter++;
//                    if(counter % 1000 == 0) {
//                        System.out.println("Continue: " + counter + " " + feature1 + "_" + feature2 + "_" + feature3);
//                    }
                }
            }
        }

        printTimeToAFile(RUNTIME_BASE + "THREE/" + feature1.getType().substring(0, 2)
        		+ "_" + feature2.getType().substring(0, 2)
        		+ "_" + feature3.getType().substring(0, 2) + ".csv", totalJaccardPlusTime, totalJaccardStarTime, totalJaccardTime, totalOmaxTime, totalOminTime);
        System.out.println("jaccard: " + ((totalJaccard)) + " jaccard Star: " + ((totalJaccardStar)) );
    }

    public static void calculateAndPrintStatistics(int q) throws FileNotFoundException, UnsupportedEncodingException {

//        for(EventType e: instanceMap.keySet()) {
//
//            PrintWriter volWriter = new PrintWriter("Vol_" + e.toString() + ".csv", "UTF-8");
//            PrintWriter lifeSpanWriter = new PrintWriter("LifeSpan_" + e.toString() + ".csv", "UTF-8");
//            PrintWriter areaWriter = new PrintWriter("Area_" + e.toString() + ".csv", "UTF-8");
//
//            for( Instance i : instanceMap.get(e)) {
//                double volume = i.getTrajectory().getVolume();
//                volWriter.println(volume);
//                double lifeSpan = (i.getInterval().getLifespan())/ONE_MINUTE;
//                lifeSpanWriter.println(lifeSpan);
//                for(TGPair tg: i.getTrajectory().getTGPairs()) {
//                    double area = tg.getGeometry().getArea();
//                    areaWriter.println(area);
//                }
//            }
//            volWriter.close();
//            lifeSpanWriter.close();
//            areaWriter.close();
//        }

        PrintWriter countWriter = new PrintWriter("count" + q  + ".csv", "UTF-8");
        int numberOfInstances = 0;
        int numberOfTGPairs = 0;
        for(EventType e: instanceMap.keySet()) {

            numberOfInstances += instanceMap.get(e).size();
            for( Instance i : instanceMap.get(e)) {
                numberOfTGPairs += i.getTrajectory().getTGPairSize();
            }
        }

        countWriter.println("Number of Instances: " + numberOfInstances);
        countWriter.println("Number of TGPairs: " + numberOfTGPairs);
        countWriter.close();
    }
}
