package edu.gsu.dmlab.solgrind.algo.measures.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeSet;

import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;
import com.vividsolutions.jts.geom.Geometry;

import edu.gsu.dmlab.solgrind.base.Instance;
import javafx.util.Pair;

/**
 * This class generates analysis for two sequences.
 * Also, class contains static method that helps one to write simple stats
 * which are jaccard/jaccard* and runtimes of those.
 *
 *
 */
public class SignificanceAnalysis {

    //CONSTRUCTORE VARIABLES
    private Instance i1;
    private Instance i2;
    private double jaccard;
    private double jaccardStar;
    private long jaccardTime;
    private long jaccardStarTime;

    //STAT VARIABLES
    private String time;
    private String intersection;
    private String union;
    private String aLive;
    private String bLive;
    private String intersecting;

    public SignificanceAnalysis(Instance i1, Instance i2,
    						double jaccard, double jaccardStar, 
    						long jaccardTime, long jaccardStarTime) 
    								throws FileNotFoundException, UnsupportedEncodingException {
        this.i1 = i1;
        this.i2 = i2;
        this.jaccard = jaccard;
        this.jaccardStar = jaccardStar;
        this.jaccardTime = jaccardTime;
        this.jaccardStarTime = jaccardStarTime;
        calculateStatsAndPrint();
    }

    public void calculateStatsAndPrint() throws FileNotFoundException, UnsupportedEncodingException {

        Set<Long> allTimes = new TreeSet<>(Sets.union(TGPairMapper.map(i1).keySet(), 
        									TGPairMapper.map(i2).keySet()));


        time = "(" + i1.getId() + "\t" + i2.getId() + ")";
        intersection = "intersection";
        intersecting = "isIntersecting";
        union = "union";
        aLive = "isLive(" + i1.getId() + ")" ;
        bLive = "isLive(" + i2.getId() + ")" ;

        for(Long l : allTimes) {
            time += "," + l;
            Geometry g1 = TGPairMapper.map(i1).get(l);
            Geometry g2 = TGPairMapper.map(i2).get(l);

            if(g1 == null && g2 == null) {
                System.out.println("both null");
            }

            if(g1 == null) {
                aLive += ",0";
                bLive += ",1";
                intersection += ",0.0";
                intersecting += ",0";
                union += "," + g2.getArea();
                continue;

            } else if(g2 == null) {
                aLive += ",1";
                bLive += ",0";
                intersection += ",0";
                intersecting += ",0";
                union += "," + g1.getArea();
                continue;
            } else {

                double intersectionArea = g1.intersection(g2).getArea();
                intersection += "," + intersectionArea;
                union += "," + g1.union(g2).getArea();
                aLive += ",1";
                bLive += ",1";
                if(intersectionArea > 0.0) {
                    intersecting += ",1";
                } else {
                    intersecting += ",0";
                }
            }

        }

        writeToFile();

    }

    public void writeToFile() throws FileNotFoundException, UnsupportedEncodingException {

        String dirName = SignificanceAnalysisRunner.ANALYSIS_BASE + "TWO/" 
        					+ i1.getType().toString().substring(0,2) + "_" 
        					+ i2.getType().toString().substring(0,2) + "/";
        File file = new File(dirName);
        file.mkdirs();
        String fileName = dirName + i1.getId() + "_" + i2.getId() + "_stats.csv";
        PrintWriter writer = new PrintWriter(fileName, "UTF-8");
        writer.println(time);
        writer.println(intersection);
        writer.println(union);
        writer.println(aLive);
        writer.println(bLive);
        writer.println(intersecting);
        writer.println("Jaccard," + jaccard);
        writer.println("JaccardTime," + jaccardTime);
        writer.println("JaccardStar," + jaccardStar);
        writer.println("JaccardStarTime," + jaccardStarTime);
        writer.close();

    }

    public static void writeAnalysisForTwo(Instance i1, Instance i2, SortedMap<String, Pair<Double, Long>> analysis) throws FileNotFoundException, UnsupportedEncodingException {

        String dirName = SignificanceAnalysisRunner.ANALYSIS_BASE + "TWO/"
                + i1.getType().toString().substring(0,2) + "_"
                + i2.getType().toString().substring(0,2) + "/";
        File file = new File(dirName);
        file.mkdirs();
        String fileName = dirName + i1.getId() + "_" + i2.getId() + "_stats.csv";
        PrintWriter writer = new PrintWriter(fileName, "UTF-8");
        for(Map.Entry<String, Pair<Double, Long>> entry: analysis.entrySet()) {
            writer.println(entry.getKey() + "," + entry.getValue().getKey() + "," + entry.getValue().getValue());
        }
        writer.close();

    }


    public static void writeAnalysisForThree(Instance i1, Instance i2, Instance i3, SortedMap<String, Pair<Double, Long>> analysis) throws Exception {
        String dirName = SignificanceAnalysisRunner.ANALYSIS_BASE + "/THREE/"
        		+ i1.getType().toString().substring(0,2) + "_" 
        		+ i2.getType().toString().substring(0,2) + "_" 
        		+ i3.getType().toString().substring(0,2) + "/";
        File file = new File(dirName);
        file.mkdirs();
        String fileName = dirName + i1.getId() + "_" + i2.getId() + "_" + i3.getId() + "_stats.csv";
        PrintWriter writer = new PrintWriter(fileName, "UTF-8");
        for(Map.Entry<String, Pair<Double, Long>> entry: analysis.entrySet()) {
            writer.println(entry.getKey() + "," + entry.getValue().getKey() + "," + entry.getValue().getValue());
        }
        writer.close();
    }

}
