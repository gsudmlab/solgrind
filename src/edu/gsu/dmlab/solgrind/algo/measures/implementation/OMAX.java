package edu.gsu.dmlab.solgrind.algo.measures.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.google.common.collect.Sets;
import com.vividsolutions.jts.geom.Geometry;

import edu.gsu.dmlab.solgrind.algo.measures.util.TGPairMapper;
import edu.gsu.dmlab.solgrind.base.Instance;

public class OMAX {


    public static double omax(Instance i1, Instance i2) {

        double intersectionVolume = Jaccard.intersection(i1, i2);

        if(intersectionVolume == 0) return 0;

        double maxArea = Math.max(volume(i1), volume(i2));

        return intersectionVolume / maxArea;
    }

    public static double omax(List<Instance> instances) throws Exception {

        if(instances.size() == 2) return omax(instances.get(0), instances.get(1));

        Set<Long> coTimeIntervals = Jaccard.findCoTimeIntervals(instances);
        if(coTimeIntervals.isEmpty()) return 0;

        Set<Long> timeIntervalList = new TreeSet<>();

        //For each combination in the list
        for(int i = 0; i < instances.size(); i++) {
            timeIntervalList = Sets.union(timeIntervalList, TGPairMapper.map(instances.get(i)).keySet());
        }

        double totalIntersectionArea = 0;
        double maxVolume = 0;

        List<Geometry> geometries = new ArrayList<>();
        for(Long t: timeIntervalList) {
            boolean isAllIntersecting = true;
            for(Instance instance:instances) {
                Geometry g1 = TGPairMapper.map(instance).get(t);
                if(g1 != null) {
                    geometries.add(g1);
                } else {
                    isAllIntersecting = false;
                }
            }
            if(isAllIntersecting) {
                totalIntersectionArea += Jaccard.findIntersectionArea(geometries);
            }
            geometries.clear();
        }

        for(Instance i : instances) {
            maxVolume = Math.max(maxVolume, volume(i));
        }

        return totalIntersectionArea / maxVolume;
    }

    public static double volume(Instance i) {

        double v = 0;
        for(Geometry g: TGPairMapper.map(i).values()){
            v += g.getArea();
        }
        return v;
    }
	
}
