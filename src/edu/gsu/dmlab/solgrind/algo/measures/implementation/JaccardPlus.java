package edu.gsu.dmlab.solgrind.algo.measures.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Sets;
import com.vividsolutions.jts.geom.Geometry;

import edu.gsu.dmlab.solgrind.algo.measures.util.TGPairMapper;
import edu.gsu.dmlab.solgrind.base.Instance;

public class JaccardPlus {
    public static double jaccardPlus(Instance i1, Instance i2) {
        return JaccardStar.jaccardStar(i1, i2);
    }

    public static double jaccardPlus2(Instance i1, Instance i2) {
        return JaccardStar.jaccardStar2(i1, i2);
    }


    public static double jaccardPlus(List<Instance> instances) throws Exception {

        if(instances.size() == 2) return JaccardStar.jaccardStar2(instances.get(0), instances.get(1));

        Set<Long> coexistenceTimeIntervals = findCoexistenceTimeIntervals(instances);

        double totalIntersectionVolume = 0.0;
        double totalUnionVolume = 0.0;

        List<Geometry> geometries = new ArrayList<>();
        for(Long t: coexistenceTimeIntervals) {
            for(Instance instance:instances) {
                Geometry g1 = TGPairMapper.map(instance).get(t);
                if(g1 != null) {
                    geometries.add(g1);
                }
            }
            double intersectionAtT = Jaccard.findIntersectionArea(geometries);
            totalIntersectionVolume += intersectionAtT;

            if(intersectionAtT > 0) {
                totalUnionVolume += Jaccard.findUnionArea(geometries);
            }
            geometries.clear();
        }

        if(totalIntersectionVolume == 0){
            return 0;
        }
        return totalIntersectionVolume / totalUnionVolume;
    }

    public static Set<Long> findCoexistenceTimeIntervals(List<Instance> instances) {

        Set<Long> temporalIntersection = TGPairMapper.map(instances.get(0)).keySet();
        for (int i = 1; i < instances.size(); i++) {
            temporalIntersection = Sets.intersection(TGPairMapper.map(instances.get(i)).keySet(), 
            		temporalIntersection);
        }

        return temporalIntersection;
    }
}
