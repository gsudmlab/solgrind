package edu.gsu.dmlab.solgrind.algo.measures.significance;

import java.util.ArrayList;
import java.util.Collection;

import edu.gsu.dmlab.solgrind.algo.measures.SignificanceMeasure;
import edu.gsu.dmlab.solgrind.algo.measures.implementation.JaccardStar;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;

/**
 * J+ (JaccardPlus) is a measure developed for alleviating the problems attached to original
 * spatiotemporal Jaccard measure. The Jaccard measure unfairly treats when the instances
 * have significantly different spatiotemporal characteristics.
 * 
 * J+ is not an antimonotonic measure and should be used in mining algorithms with caution. 
 * 
 * This implementation uses the algorithms in 
 * Berkay Aydin, Ahmet Kucuk, and Rafal Angryk 
 * "Measuring the Significance of Spatiotemporal Co-occurrences" ACM TSAS
 * 
 * @author berkay
 * 
 */
public class JaccardPlus implements SignificanceMeasure {

	@Override
	public double calculate(Instance ins1, Instance ins2) {
		return edu.gsu.dmlab.solgrind.algo.measures.implementation.
				JaccardPlus.jaccardPlus(ins1, ins2);
	}

	@Override
	public double calculate(Collection<Instance> instances) {
		try {
			return edu.gsu.dmlab.solgrind.algo.measures.implementation.
					JaccardPlus.jaccardPlus(new ArrayList<>(instances));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0.0;
	}

	@Override
	public double calculateT(Trajectory traj1, Trajectory traj2) {
		Instance i1 = new Instance(null, null, traj1); 
		Instance i2 = new Instance(null, null, traj2);
		JaccardStar.jaccardStar2(i1, i2);
		return 0;
	}

	@Override
	public double calculateT(Collection<Trajectory> trajectories) {
		ArrayList<Instance> instances = new ArrayList<>();
		for(Trajectory traj : trajectories){
			instances.add(new Instance(null, null, traj));
		}
		try {
			return edu.gsu.dmlab.solgrind.algo.measures.implementation.JaccardPlus.jaccardPlus(instances);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

}
