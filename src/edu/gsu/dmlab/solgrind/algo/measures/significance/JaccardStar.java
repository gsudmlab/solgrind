package edu.gsu.dmlab.solgrind.algo.measures.significance;

import com.vividsolutions.jts.geom.Geometry;

import edu.gsu.dmlab.solgrind.algo.measures.SignificanceMeasure;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.types.essential.TGPair;
import edu.gsu.dmlab.solgrind.base.types.essential.TInterval;
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;
import edu.gsu.dmlab.solgrind.base.types.instance.InstanceCooccurrence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * J* (JaccardStar) is an antimonotonic measure developed for alleviating the problems 
 * attached to original spatiotemporal Jaccard measure as well as the J+ measure. 
 * The Jaccard measure unfairly treats when the instances have significantly different 
 * spatiotemporal characteristics. J+ is not antimonotonic. J* is a middle-ground.
 * 
 * This implementation uses the algorithms in 
 * Berkay Aydin, Ahmet Kucuk, and Rafal Angryk 
 * "Measuring the Significance of Spatiotemporal Co-occurrences" ACM TSAS
 * 
 * Created by ahmetkucuk on 30/09/16.
 * @author berkay
 */
public class JaccardStar implements SignificanceMeasure {

    public static Geometry getGeomFromSet(Set<TGPair> set, TInterval interval) {
        for(TGPair tgPair: set) {
            if(tgPair.getTInterval().equals(interval)) return tgPair.getGeometry();
        }
        return null;
    }

	@Override
	public double calculate(Instance ins1, Instance ins2) {
		return edu.gsu.dmlab.solgrind.algo.measures.implementation.
				JaccardStar.jaccardStar2(ins1, ins2);
	}

	@Override
	public double calculate(Collection<Instance> instances) {
		try {
			return edu.gsu.dmlab.solgrind.algo.measures.implementation.
			JaccardStar.jaccardStar(new ArrayList<>(instances));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public double calculateT(Trajectory traj1, Trajectory traj2) {
		return edu.gsu.dmlab.solgrind.algo.measures.implementation.
				JaccardStar.jaccardStar(new Instance(null, null, traj1),
										new Instance(null, null, traj2));
	}

	@Override
	public double calculateT(Collection<Trajectory> trajectories) {
		ArrayList<Instance> instances = new ArrayList<>();
		for(Trajectory traj : trajectories){
			instances.add(new Instance(null, null, traj));
		}
		try {
			return edu.gsu.dmlab.solgrind.algo.measures.implementation.
					JaccardStar.jaccardStar(instances);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	


}
