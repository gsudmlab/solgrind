package edu.gsu.dmlab.solgrind.algo.measures.significance;

import java.util.ArrayList;
import java.util.Collection;

import edu.gsu.dmlab.solgrind.algo.measures.SignificanceMeasure;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.operations.STOperations;
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;


/**
 * This is the spatiotemporal version of Jaccard measure. The Jaccard
 * measure calculates the ratio between the intersection volume of instances
 * to the union volume of instances.
 * 
 * The measure originally appeared in 
 * Karthik Ganesan Pillai, Rafal A Angryk, Berkay Aydin
 * "A filter-and-refine approach to mine spatiotemporal co-occurrences" SIGSPATIAL-2013
 * 
 * This implementation uses the version in 
 * Berkay Aydin, Ahmet Kucuk, and Rafal Angryk "Measuring the Significance of Spatiotemporal Co-occurrences" ACM TSAS
 * 
 * @author berkay
 */
public class Jaccard implements SignificanceMeasure {

	@Override
	public double calculate(Instance ins1, Instance ins2) {
		return edu.gsu.dmlab.solgrind.algo.measures.implementation.
				Jaccard.jaccard(ins1, ins2);
	}

	@Override
	public double calculate(Collection<Instance> instances) {
		
		ArrayList<Instance> instanceList = new ArrayList<>(instances);
		try {
			return edu.gsu.dmlab.solgrind.algo.measures.implementation.
					Jaccard.jaccard(instanceList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0.0;
	}

	@Override
	public double calculateT(Trajectory traj1, Trajectory traj2) {
		double numerator = STOperations.intersection(traj1, traj2).getVolume();
		double denominator = STOperations.union(traj1, traj2).getVolume();
		
		return numerator/denominator;
	}

	@Override
	public double calculateT(Collection<Trajectory> trajectories) {
		double numerator = STOperations.intersectionAll(trajectories).getVolume();
		double denominator = STOperations.unionAll(trajectories).getVolume();
		
		return numerator/denominator;
	}

}
