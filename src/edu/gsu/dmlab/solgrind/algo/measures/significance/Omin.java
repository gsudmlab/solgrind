package edu.gsu.dmlab.solgrind.algo.measures.significance;

import java.util.Collection;
import java.util.HashSet;

import edu.gsu.dmlab.solgrind.algo.measures.SignificanceMeasure;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.operations.STOperations;
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;
/**
 * This class calculates OMIN (Overlap min measure). Omin is not antimonotonic.
 * Thus, use it with caution in pattern mining algorithms.
 *
 * @author berkay
 *
 */
public class Omin implements SignificanceMeasure{

	@Override
	public double calculate(Instance ins1, Instance ins2) {
		Trajectory traj1 = ins1.getTrajectory();
		Trajectory traj2 = ins2.getTrajectory();
		
		double volume1 = traj1.getVolume();
		double volume2 = traj2.getVolume();
		double minVolume = Math.min(volume1, volume2);

		double intersectionVolume = STOperations.intersection(traj1, traj2).getVolume(); 
		
		return intersectionVolume/minVolume;
	}

	@Override
	public double calculate(Collection<Instance> instances) {
		double minVolume = 0.0;
		
		for( Instance ins : instances ){
			minVolume = Math.min(minVolume, ins.getTrajectory().getVolume());
		}
		
		HashSet<Trajectory> trajectories = new HashSet<Trajectory>();
		for( Instance ins : instances ){
			trajectories.add(ins.getTrajectory());
		}
		double intersectionVolume = STOperations.intersectionAll(trajectories).getVolume();
		
		return intersectionVolume/minVolume;
	}

	@Override
	public double calculateT(Trajectory traj1, Trajectory traj2) {
		double volume1 = traj1.getVolume();
		double volume2 = traj2.getVolume();
		double minVolume = Math.min(volume1, volume2);

		double intersectionVolume = STOperations.intersection(traj1, traj2).getVolume(); 
		
		return intersectionVolume/minVolume;
	}

	@Override
	public double calculateT(Collection<Trajectory> trajectories) {
		double denom = 0.0;
		for( Trajectory traj : trajectories ){
			denom = Math.min(denom, traj.getVolume());
		}

		double numerator = STOperations.intersectionAll(trajectories).getVolume();
		
		return numerator/denom;
	}

}
