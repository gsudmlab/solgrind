package edu.gsu.dmlab.solgrind.algo.measures;

import java.util.Collection;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;

/**
 * Significance measure calculation
 * @author berkay - Aug 16, 2016
 * 
 * This is the abstract class that we use to calculate the significance measures
 * for spatiotemporal co-occurrence relationship among trajectories.
 * 
 * The list of measures and their algorithms can be found in the following paper:
 * Berkay Aydin, Ahmet Kucuk, and Rafal Angryk
 * "Measuring the Significance of Spatiotemporal Co-occurrences" ACM TSAS
 */

public interface SignificanceMeasure {

	
	public double calculate( Instance ins1, Instance ins2 );
	public double calculate( Collection<Instance> instances);
	
	public double calculateT( Trajectory traj1, Trajectory traj2);
	public double calculateT( Collection<Trajectory> trajectories);

}
	

