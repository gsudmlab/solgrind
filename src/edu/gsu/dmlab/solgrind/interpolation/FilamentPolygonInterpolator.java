package edu.gsu.dmlab.solgrind.interpolation;

import java.util.TreeSet;

import com.vividsolutions.jts.geom.Geometry;

import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.types.essential.TGPair;

public class FilamentPolygonInterpolator extends Interpolator{

	@Override
	public Instance interpolate(Instance ins) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isValidEventType(EventType e) {
		return e.getType().equals("FI");
	}

	@Override
	public TreeSet<TGPair> interpolateTGPair(TGPair tgp, TGPair nextTgp) {
		// TODO Auto-generated method stub
		return null;
	}

}
