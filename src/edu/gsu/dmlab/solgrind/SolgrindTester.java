package edu.gsu.dmlab.solgrind;

import edu.gsu.dmlab.solgrind.experiment.S3COPExperiments;
import edu.gsu.dmlab.solgrind.experiment.STESExperiments;

public class SolgrindTester {

	private static final long INTERVAL = 1;
	private static final String ARTIFICIAL_DATASET = "/Users/ahmetkucuk/Documents/Research/BERKAY/DS_forAhmet/AD%d/";
	private static final String EXPORT_FILE_CO = "/Users/ahmetkucuk/Documents/Research/BERKAY/experiments/graphs/1month/CO.dot";
	private static final String GRAPHS_DIR = "/Users/ahmetkucuk/Documents/Research/BERKAY/experiments/graphs/AD%d/";
//	private static final String RESULTS_DIR = "/Users/ahmetkucuk/Documents/Research/BERKAY/experiments/results/AD%d/";
//	private static final String EXPORT_FILE_SQ = "/Users/ahmetkucuk/Documents/Research/BERKAY/experiments/graphs/1month/SQ.dot";
	private static final String EXPORT_FILE_SQ = "./experiments/graphs/1month/SQ.dot";

	private static final String DATA_DIR = "/Users/ahmetkucuk/Documents/Research/BERKAY/interpolated_2012_v2/";
//	private static final String RESULTS_DIR = "/Users/ahmetkucuk/Documents/Research/BERKAY/experiments/results/";
	private static final String RESULTS_DIR = "./experiments/results/";

	public static final String[] argv = new String[]{DATA_DIR, "0", RESULTS_DIR};
	public static void main(String[] args) throws Exception {
//		STESExperiments.start(args);
		//STESExperiments.collectStats(args);
		
		String dataDir = "./datasets/toy/";		
		String resultDir = "./experiments/results/";
		String[] argv2 = new String[]{dataDir, resultDir};
		
		S3COPExperiments.experimentsForS3COP(argv2[0], argv2[1]);
	}

	public static void run() {

	}

}
