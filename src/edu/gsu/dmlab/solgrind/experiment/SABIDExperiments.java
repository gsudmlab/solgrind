package edu.gsu.dmlab.solgrind.experiment;

import com.vividsolutions.jts.io.ParseException;
import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.database.query.QueryUtil;
import edu.gsu.dmlab.solgrind.experiment.util.SolgrindFileUtils;
import edu.gsu.dmlab.solgrind.index.GraphIndex;
import edu.gsu.dmlab.solgrind.index.InstanceVertex;

import java.util.Set;

/**
 * Created by ahmet on 10/14/16.
 */
public class SABIDExperiments {


    static final String[] eventTypeArray = {"ar", "ch", "ss", "fi", "fl", "sg", "ef"};
    public static void experimentsForSubid() throws ParseException {
        GraphIndex solgrind = new GraphIndex(eventTypeArray);

//		t1 = System.currentTimeMillis();
//		solgrind.buildEventSequenceGraph();
//		t2 = System.currentTimeMillis();
//		System.out.println("Build EventEventSequence Graph took " + (t2-t1)/1000 + " seconds");


        long cooccurenceFollowedBy = 0;
        long cooccurenceWith = 0;
        long followedBy = 0;
        for(int i = 0; i < 100; i++) {

            long t1 = System.currentTimeMillis();
            QueryUtil.findCooccurAndFollowedBy(solgrind, new EventType("ar"), new EventType("ss"), new EventType("ar"));
            long t2 = System.currentTimeMillis();
            cooccurenceFollowedBy += t2-t1;
            t1 = System.currentTimeMillis();
            QueryUtil.findCooccurWith(solgrind, new EventType("fi"), new EventType("ch"), 0.1);
            t2 = System.currentTimeMillis();
            cooccurenceWith += t2 - t1;
            t1 = System.currentTimeMillis();
            QueryUtil.findFollowedBy(solgrind, new EventType("ar"), new EventType("sg"), 0.1);
            t2 = System.currentTimeMillis();
            followedBy += t2-t1;
        }

        Set<InstanceVertex> cooccurAndFollowedBys = QueryUtil.findCooccurAndFollowedBy(solgrind, new EventType("sg"), new EventType("ar"), new EventType("ss"));
        System.out.println(cooccurAndFollowedBys.size());
        System.out.println(cooccurAndFollowedBys);
        System.out.println("Find CooccurAndFollowedBy " + (cooccurenceFollowedBy)/100.0 + " ms");

        Set<InstanceVertex> cooccurances = QueryUtil.findCooccurWith(solgrind, new EventType("fi"), new EventType("ch"), 0.1);
        System.out.println(cooccurances.size());
        System.out.println(cooccurances);
        System.out.println("Find findCooccurWith " + (cooccurenceWith)/100.0 + " ms");

        Set<InstanceVertex> followed = QueryUtil.findFollowedBy(solgrind, new EventType("ar"), new EventType("sg"), 0.1);
        System.out.println(followed.size());
        System.out.println(followed);
        System.out.println("Find findFollowedBy " + (followedBy)/100.0 + " ms");
//		System.out.println(QueryUtil.findCooccurAndFollowedBy(solgrind, new EventType("ss"), new EventType("ar"), new EventType("ss")));


    }

}
