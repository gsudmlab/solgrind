package edu.gsu.dmlab.solgrind.experiment;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import com.vividsolutions.jts.io.ParseException;

import edu.gsu.dmlab.solgrind.algo.stcopminers.S3copMiner;
import edu.gsu.dmlab.solgrind.algo.stcopminers.StcopMiner;
import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.types.essential.TInterval;
import edu.gsu.dmlab.solgrind.database.query.QueryUtil;
import edu.gsu.dmlab.solgrind.fileio.CSVDataParser;
import edu.gsu.dmlab.solgrind.index.GraphIndex;
import edu.gsu.dmlab.solgrind.index.InstanceVertex;

public class S3COPExperiments {

	static final TInterval[] tiArray = new TInterval[] {
            new TInterval("2012-01-01 00:00:00", "2012-01-31 23:59:59")
//            ,
//            new TInterval("2012-02-01 00:00:00", "2012-02-28 23:59:59"),
//            new TInterval("2012-03-01 00:00:00", "2012-03-31 23:59:59"),
//            new TInterval("2012-04-01 00:00:00", "2012-04-30 23:59:59"),
//            new TInterval("2012-05-01 00:00:00", "2012-05-31 23:59:59"),
//            new TInterval("2012-06-01 00:00:00", "2012-06-30 23:59:59"),
//            new TInterval("2012-07-01 00:00:00", "2012-07-31 23:59:59"),
//            new TInterval("2012-08-01 00:00:00", "2012-08-31 23:59:59"),
//            new TInterval("2012-09-01 00:00:00", "2012-09-30 23:59:59"),
//            new TInterval("2012-10-01 00:00:00", "2012-10-31 23:59:59"),
//            new TInterval("2012-11-01 00:00:00", "2012-11-30 23:59:59"),
//            new TInterval("2012-12-01 00:00:00", "2012-12-31 23:59:59")
    };
	
	
//	static final String[] eventTypeArray = {"ar", "ss", "fl", "sg", "ef", "ch"};
	static final String[] eventTypeArray = {"ss", "ar", "fl", "sg"};
	public static void experimentsForS3COP(String datasetPath, String outputdir) throws ParseException {
		Map<EventType, Set<Instance>> instanceMap = new CSVDataParser(datasetPath).
        		readToInstanceMap(createEventTypes(eventTypeArray), tiArray[0]);

//		new StcopMiner(instanceMap);
		new S3copMiner(instanceMap);
		

//		long cooccurenceFollowedBy = 0;
//		long cooccurenceWith = 0;
//		long followedBy = 0;
//		for(int i = 0; i < 100; i++) {
//
//			long t1 = System.currentTimeMillis();
//			QueryUtil.findCooccurAndFollowedBy(solgrind, new EventType("ar"), new EventType("ss"), new EventType("ar"));
//			long t2 = System.currentTimeMillis();
//			cooccurenceFollowedBy += t2-t1;
//			t1 = System.currentTimeMillis();
//			QueryUtil.findCooccurWith(solgrind, new EventType("fi"), new EventType("ch"), 0.1);
//			t2 = System.currentTimeMillis();
//			cooccurenceWith += t2 - t1;
//			t1 = System.currentTimeMillis();
//			QueryUtil.findFollowedBy(solgrind, new EventType("ar"), new EventType("sg"), 0.1);
//			t2 = System.currentTimeMillis();
//			followedBy += t2-t1;
//		}
//
//		Set<InstanceVertex> cooccurAndFollowedBys = QueryUtil.findCooccurAndFollowedBy(solgrind, new EventType("sg"), new EventType("ar"), new EventType("ss"));
//		System.out.println(cooccurAndFollowedBys.size());
//		System.out.println(cooccurAndFollowedBys);
//		System.out.println("Find CooccurAndFollowedBy " + (cooccurenceFollowedBy)/100.0 + " ms");
//
//		Set<InstanceVertex> cooccurances = QueryUtil.findCooccurWith(solgrind, new EventType("fi"), new EventType("ch"), 0.1);
//		System.out.println(cooccurances.size());
//		System.out.println(cooccurances);
//		System.out.println("Find findCooccurWith " + (cooccurenceWith)/100.0 + " ms");
//
//		Set<InstanceVertex> followed = QueryUtil.findFollowedBy(solgrind, new EventType("ar"), new EventType("sg"), 0.1);
//		System.out.println(followed.size());
//		System.out.println(followed);
//		System.out.println("Find findFollowedBy " + (followedBy)/100.0 + " ms");
//		System.out.println(QueryUtil.findCooccurAndFollowedBy(solgrind, new EventType("ss"), new EventType("ar"), new EventType("ss")));


	}
	
	private static ArrayList<EventType> createEventTypes(String[] eventTypeArray) {
        ArrayList<EventType> eventTypes = new ArrayList<>();
        for(String etStr : eventTypeArray){
            eventTypes.add(new EventType(etStr));
        }
        return eventTypes;
    }

}
