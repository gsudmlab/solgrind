package edu.gsu.dmlab.solgrind.experiment;

import java.util.HashSet;
import java.util.Set;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.types.essential.TGPair;
import edu.gsu.dmlab.solgrind.base.types.essential.Trajectory;
import edu.gsu.dmlab.solgrind.fileio.CSVDataParser;
import edu.gsu.dmlab.solgrind.fileio.CSVDataWriter;
import edu.gsu.dmlab.solgrind.interpolation.ComplexPolygonInterpolator;
import edu.gsu.dmlab.solgrind.interpolation.Interpolator;

public class InterpolationTester {

	static final String geom1 = "POLYGON((-128.1 -326.1,-127.5 -325.5,-107.1 -260.7,-87.9 -266.7,-77.7 -330.9,-90.9 -354.3,-128.1 -326.1))";
	static final String geom2 = "POLYGON((-104.1 -284.7,-103.5 -284.1,-93.3 -254.1,-56.1 -258.3,-70.5 -330.3,-87.9 -329.1,-104.1 -284.7))";
	static final String geom3 = "POLYGON((-169.5 -298.5,-168.9 -297.9,-140.7 -272.7,-78.9 -284.1,-54.9 -245.1,-25.5 -255.3,-36.3 -284.7,-17.7 -316.5,-30.3 -332.7,-63.9 -329.1,-113.7 -378.3,-150.3 -356.1,-100.5 -354.9,-84.3 -309.9,-110.1 -291.9,-169.5 -298.5))";
	static final String geom4 = "POLYGON((-68.7 -288.9,-68.1 -288.3,-36.3 -284.1,-11.1 -255.3,-2.1 -297.3,-45.3 -321.9,-68.7 -288.9))";
	
	static final String mbr1 = "POLYGON((-322.773 177.8082,-242.6604 180.2562,-239.9646 260.6166,-319.188 258.1866,-322.773 177.8082))";
	static final String mbr2 = "POLYGON((-270.879 174.8586,-171.6342 177.198,-169.1862 276.5514,-267.0162 274.2342,-270.879 174.8586))";
	static final String mbr3 = "POLYGON((-254.2806 175.2558,-155.7528 177.4014,-153.558 275.9784,-250.698 273.8532,-254.2806 175.2558))";
	static final String mbr4 = "POLYGON((-243.5574 176.3526,-144.0408 178.398,-141.9714 277.9386,-240.0582 275.9136,-243.5574 176.3526))";
	
	static final String fig1 = "POLYGON((-322.773 177.8082,-242.6604 180.2562,-239.9646 260.6166,-319.188 258.1866,-322.773 177.8082))";
	static final String fig2 = "POLYGON((-270.879 174.8586,-171.6342 177.198,-169.1862 276.5514,-267.0162 274.2342,-270.879 174.8586))";
	static final String fig3 = "POLYGON((-254.2806 175.2558,-155.7528 177.4014,-153.558 275.9784,-250.698 273.8532,-254.2806 175.2558))";
	static final String fig4 = "POLYGON((-243.5574 176.3526,-144.0408 178.398,-141.9714 277.9386,-240.0582 275.9136,-243.5574 176.3526))";
	
	static final String[] eventTypeNames = {"ar", "ch", "ef", "fi", "fl", "sg", "ss"};
	
	
	public static void driver(String[] args) {
//		System.out.println("Original MBR-based instance:\n" + mbrInstance().toString() );
//		Instance i_mbr_ins = new MBRInterpolator().interpolate(mbrInstance());
//		System.out.println("Interpolated:\n" + i_mbr_ins);
		
/*		System.out.println("Original Polygon-based instance:\n" + polyInstance().toString() );
		Instance i_poly_ins = new ComplexPolygonInterpolator().interpolate(polyInstance());
		System.out.println("Interpolated:\n" + i_poly_ins);
		
		for(TGPair tgp : i_poly_ins.getTrajectory().getTGPairs()){
			System.out.println(tgp.getGeometry().getNumPoints());
		}*/
		CSVDataParser csvParser = new CSVDataParser();
		CSVDataWriter csvWriter = new CSVDataWriter();
		Interpolator itp = null;
		for(String etName : eventTypeNames){
			if(!etName.equalsIgnoreCase("ss")){
				continue;
			}
			Set<Instance> eventInstancesSet = csvParser.readInstances(new EventType(etName));
			Set<Instance> interpolatedInstanceSet = new HashSet<>(); 
			for(Instance eventInstance : eventInstancesSet){
				//System.out.println("Original SS instance: " + eventInstance);
				itp = Interpolator.selectInterpolator(eventInstance.getType());
				Instance iEventInstance = itp.interpolate(eventInstance);
				interpolatedInstanceSet.add(iEventInstance);
				//System.out.println("Interpolated::: \t" + iEventInstance );
			}
			csvWriter.write(interpolatedInstanceSet, etName);
		}
		System.out.println("Fixed Empty geometry count::: " + ComplexPolygonInterpolator.empty_count);
		System.out.println("Areal invalid count::: " + ComplexPolygonInterpolator.arealInvalid_count);

		
		
//		csvParser.readInstances(new EventType("ar"));
//		csvParser.readInstances(new EventType("ch"));
//		csvParser.readInstances(new EventType("ef"));
//		csvParser.readInstances(new EventType("fi"));
//		csvParser.readInstances(new EventType("fl"));
		//System.out.println(csvParser.readInstances(new EventType("sg")) );
		//System.out.println(csvParser.readInstances(new EventType("ss")) );
		
//		Set<Instance> event_InstancesMap = csvParser.readInstances(new EventType("fi"));
//		System.out.println("Number of SS instances: " + SS_InstancesMap.size());
//		Instance ssInstance = (Instance) SS_InstancesMap.toArray()[0] ;
//		System.out.println("Original SS instance: " + ssInstance);
//		ComplexPolygonInterpolator cpi = new ComplexPolygonInterpolator();
//		Instance i_ss_instance = cpi.interpolate(ssInstance);
//		System.out.println("Interpolated::: \t" + i_ss_instance );
		
		
//		for(Instance eventInstance : event_InstancesMap){
//			//System.out.println("Original SS instance: " + eventInstance);
//			itp = Interpolator.selectInterpolator(eventInstance.getType());
//			Instance iEventInstance = itp.interpolate(eventInstance);
//			System.out.println("Interpolated::: \t" + iEventInstance );
//		}
		System.out.println("Done");
	}




	private static Instance polyInstance() {
		Instance insMBR = new Instance("1", new EventType("AR"), new Trajectory());
		
		WKTReader reader = new WKTReader();
		Geometry geom;
		try {
			geom = reader.read(geom1);
			insMBR.getTrajectory().addTGPair(new TGPair("2013-07-15 15:17:48", "2013-07-15 15:47:48", geom));
			
			geom = reader.read(geom2);
			insMBR.getTrajectory().addTGPair(new TGPair("2013-07-15 15:47:48", "2013-07-15 16:57:48", geom));
			
			geom = reader.read(geom3);
			insMBR.getTrajectory().addTGPair(new TGPair("2013-07-15 16:57:48", "2013-07-15 17:22:48", geom));
			
			geom = reader.read(geom4);
			insMBR.getTrajectory().addTGPair(new TGPair("2013-07-15 17:22:48", "2013-07-15 17:47:48", geom));
			
			geom = reader.read(geom3);
			insMBR.getTrajectory().addTGPair(new TGPair("2013-07-15 17:47:48", "2013-07-15 18:58:48", geom));
			
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return insMBR;
	}
	
	private static Instance mbrInstance() {
		Instance insMBR = new Instance("2", new EventType("SG"), new Trajectory());
		
		WKTReader reader = new WKTReader();
		Geometry geom;
		try {
			geom = reader.read(mbr1);
			insMBR.getTrajectory().addTGPair(new TGPair("2013-07-15 15:17:48", "2013-07-15 15:47:48", geom));
			
			geom = reader.read(mbr2);
			insMBR.getTrajectory().addTGPair(new TGPair("2013-07-15 15:47:48", "2013-07-15 16:57:48", geom));
			
			geom = reader.read(mbr3);
			insMBR.getTrajectory().addTGPair(new TGPair("2013-07-15 16:57:48", "2013-07-15 17:22:48", geom));
			
			geom = reader.read(mbr4);
			insMBR.getTrajectory().addTGPair(new TGPair("2013-07-15 17:22:48", "2013-07-15 17:40:00", geom));
			
			geom = reader.read(mbr4);
			insMBR.getTrajectory().addTGPair(new TGPair("2013-07-15 17:40:00", "2013-07-15 18:01:00", geom));
			
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return insMBR;
	}

}
