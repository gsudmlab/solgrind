package edu.gsu.dmlab.solgrind.experiment.util;

import edu.gsu.dmlab.solgrind.database.helper.DatasetDBLoader;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

/**
 * Created by ahmet on 10/14/16.
 */
public class SolgrindFileUtils {



    public static<S extends Comparable<S>, T> void writeResultsInMap(Map<S, T> map, Path p) {

        System.out.println("Results are written in " + p.toString());
        p.toFile().getParentFile().mkdirs();
        try(BufferedWriter writer = Files.newBufferedWriter(p)) {

            for(S es: new TreeSet<>(map.keySet())){
                writer.write(es + "\t" + map.get(es) + "\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static String[] getEventTypes(String folder) {
        List<Path> files = DatasetDBLoader.findFilesFromFolder(folder);
        String[] eventTypes = new String[files.size()];
        int i = 0;
        for(Path s: files) {
            String pathString = s.toString();
            String eventType = pathString.substring(pathString.lastIndexOf("/")+1);
            eventTypes[i] = eventType;
            i++;
        }
        return eventTypes;
    }
}
