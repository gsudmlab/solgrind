package edu.gsu.dmlab.solgrind.experiment;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import edu.gsu.dmlab.solgrind.algo.stcopminers.StcopMiner;
import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.types.essential.TInterval;
import edu.gsu.dmlab.solgrind.fileio.CSVDataParser;

public class ReaderTester {
	
	public static void main(String[] args){
		
		System.out.println("Testing the interpolated data reader");
		
		//long startTime = System.nanoTime();
		
		//String[] eventTypeArray = {"ar","ch","ef","fi","fl","sg","ss"};
		TInterval ti = new TInterval("2012-01-01 00:00:00", "2012-01-31 23:59:59");
		String[] eventTypeArray = {"ar", "ss"};
		ArrayList<EventType> eventTypes = new ArrayList<>();
		for(String etStr : eventTypeArray){
			eventTypes.add(new EventType(etStr));
		}
		Map<EventType, Set<Instance>> instanceMap = 
				new CSVDataParser().readToInstanceMap(eventTypes, ti);
		System.out.println("I have read all the instances to InstanceMap in time interval: " + ti );
		for(Entry<EventType, Set<Instance>> kv : instanceMap.entrySet()){
			System.out.println("Event Type: " + kv.getKey().toString() +
					"\t# of instances: " + kv.getValue().size());
			
//			for(Instance ins: kv.getValue()){
//				System.out.println("Instance: " + ins.getInterval());
//			}
		}
		System.out.println();
		long startTime = System.nanoTime();
		new StcopMiner(instanceMap);
		
		long endTime = System.nanoTime();
		long duration = (endTime - startTime);
		System.out.println("Duration::: " + duration);

		
		//TEST FOR READ INSTANCES
//		Set<Instance> instances = new CSVDataParser().readInstances(new EventType("ef"));
//		System.out.println("I have read " + instances.size() + " instances of type EF" );
//		for(Instance ins: instances ){
//			
//			System.out.println(ins.toString());
//			break;
//		}
		
	}
	

}
