package edu.gsu.dmlab.solgrind.experiment;

import edu.gsu.dmlab.solgrind.SolgrindConstants;
import edu.gsu.dmlab.solgrind.algo.stesminers.BtspESMiner;
import edu.gsu.dmlab.solgrind.algo.stesminers.NaiveTopRKESMiner;
import edu.gsu.dmlab.solgrind.algo.stesminers.SequenceConnectMiner;
import edu.gsu.dmlab.solgrind.algo.stesminers.ESGrowthMiner;
import edu.gsu.dmlab.solgrind.algo.stesminers.TopRKESMiner;
import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.base.types.essential.TInterval;
import edu.gsu.dmlab.solgrind.base.types.event.EventSequence;
import edu.gsu.dmlab.solgrind.database.helper.DatasetParser;
import edu.gsu.dmlab.solgrind.database.helper.DotOperations;
import edu.gsu.dmlab.solgrind.experiment.util.SolgrindFileUtils;
import edu.gsu.dmlab.solgrind.fileio.CSVDataParser;
import edu.gsu.dmlab.solgrind.index.GraphIndex;
import edu.gsu.dmlab.solgrind.index.SequenceGraph;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by ahmet on 10/14/16.
 */
public class STESExperiments {


    static final String[] eventTypeArray = {"ar", "ch", "ss", "fi", "fl", "sg", "ef"};
//    static final String[] eventTypeArray = {"sg", "ar"};

//    static final TInterval[] tiArray = new TInterval[] {
//            new TInterval("2012-01-01 00:00:00", "2012-03-31 23:59:59"),
//            new TInterval("2012-04-01 00:00:00", "2012-06-30 23:59:59"),
//            new TInterval("2012-07-01 00:00:00", "2012-09-30 23:59:59"),
//            new TInterval("2012-10-01 00:00:00", "2012-12-31 23:59:59")
//    };


    static final TInterval[] tiArray = new TInterval[] {
            new TInterval("2012-01-01 00:00:00", "2012-01-31 23:59:59"),
            new TInterval("2012-02-01 00:00:00", "2012-02-28 23:59:59"),
            new TInterval("2012-03-01 00:00:00", "2012-03-31 23:59:59"),
            new TInterval("2012-04-01 00:00:00", "2012-04-30 23:59:59"),
            new TInterval("2012-05-01 00:00:00", "2012-05-31 23:59:59"),
            new TInterval("2012-06-01 00:00:00", "2012-06-30 23:59:59"),

            new TInterval("2012-07-01 00:00:00", "2012-07-31 23:59:59"),
            new TInterval("2012-08-01 00:00:00", "2012-08-31 23:59:59"),
            new TInterval("2012-09-01 00:00:00", "2012-09-30 23:59:59"),
            new TInterval("2012-10-01 00:00:00", "2012-10-31 23:59:59"),
            new TInterval("2012-11-01 00:00:00", "2012-11-30 23:59:59"),
            new TInterval("2012-12-01 00:00:00", "2012-12-31 23:59:59")
    };

    private static final Map<String, Long> runTimes = new HashMap<>();

    public static void start(String[] args) throws IOException, InterruptedException {

        System.out.println("I am testing this");

        if(args.length < 2) {
            System.out.println("ARGS: 1 -> Data Dir, 2 -> intervalIndex, 3 -> Output dir" );
            return;
        }

        String path = args[0];
        int intervalIndex = Integer.parseInt(args[1]);
        String outputDir = args[2];
        System.out.println("path : " + path + "interval Index: " +  tiArray[intervalIndex] + " output: " + outputDir);

        Map<EventType, Set<Instance>> instanceMap = new CSVDataParser(path).readToInstanceMap(createEventTypes(eventTypeArray), tiArray[intervalIndex]);

        long t1 = System.currentTimeMillis();
        GraphIndex solgrind = new GraphIndex(eventTypeArray);
        solgrind.buildFromInstances(instanceMap, runTimes);
        long t2 = System.currentTimeMillis();
        runTimes.put("Running time TOTAL for graph building (secs): ", (t2-t1)/1000);

        System.out.println("Experiments started");
        DotOperations.toDot(solgrind.getSolgrindSequence(), outputDir + "SQ.dot");
        instanceMap.clear();
        System.gc();
        runExperiments(solgrind.getSolgrindSequence(), outputDir);


        SolgrindFileUtils.writeResultsInMap(runTimes, Paths.get(outputDir + "runtimes.txt"));

        statsOnGraph(solgrind.getSolgrindSequence(), outputDir);

    }

    public static void collectStats(String[] args) throws IOException {

        System.out.println("Collecting stats");
        if(args.length < 2) {
            System.out.println("ARGS: 1 -> Path to Folder 2 -> path to output dir" );
            return;
        }

        String pathToData = args[0];
        String outputDir = args[1];
        GraphIndex solgrind = new GraphIndex(eventTypeArray);
        solgrind.buildSeqFromDOT(outputDir + "SQ.dot");
        statsOnGraph(solgrind.getSolgrindSequence(), outputDir);
        statsOnData(pathToData, outputDir);
    }

    public static void statsOnData(String pathToData, String outputDir) throws IOException {

        Map<String, Integer> trjCountMap = new HashMap<>();

        int trjCountAll = 0;
        int tgPairCountAll = 0;

        for(String s : SolgrindFileUtils.getEventTypes(pathToData)){
            DatasetParser parser = new DatasetParser(Paths.get(pathToData + s), s, SolgrindConstants.SAMPLING_INTERVAL);

            Instance i;
            int trjCount = 0;
            int tgPairCount = 0;
            while((i = parser.next()) != null) {
                trjCount++;
                tgPairCount += i.getTrajectory().getTGPairSize();
            }
            trjCountAll += trjCount;
            tgPairCountAll += tgPairCount;

            trjCountMap.put(s, trjCount);
            trjCountMap.put(s + "_tg_pairs", tgPairCount);
        }

        trjCountMap.put("all_trj", trjCountAll);
        trjCountMap.put("all_tg_pairs", tgPairCountAll);
        SolgrindFileUtils.writeResultsInMap(trjCountMap, Paths.get(outputDir + "trajectory_stats.txt"));
    }

    public static void statsOnGraph(SequenceGraph sequenceGraph, String outputDir) {
        Map<EventType, Integer> countMap = sequenceGraph.countVerticesByEventType();
        countMap.put(new EventType("ALL"), sequenceGraph.vertexSet().size());
        countMap.put(new EventType("Edges"), sequenceGraph.edgeSet().size());
        SolgrindFileUtils.writeResultsInMap(countMap, Paths.get(outputDir + "eventCount.txt"));
    }


    public static void runExperiments(SequenceGraph sequenceGraph, String resultDir) {

    	long t1 = System.currentTimeMillis();
        runSequenceConnectExperiments(sequenceGraph, resultDir);
        long t2 = System.currentTimeMillis();
        runTimes.put("Running time for SequenceConnect Experiments (secs): ", (t2-t1)/1000);

    	
        t1 = System.currentTimeMillis();
        runEsGrowthExperiments(sequenceGraph, resultDir);
        t2 = System.currentTimeMillis();

        runTimes.put("Running time for EsGrowth Experiments (secs): ", (t2-t1)/1000);

        t1 = System.currentTimeMillis();
        runTopRKNaiveExperiments(sequenceGraph, resultDir);
        t2 = System.currentTimeMillis();
        runTimes.put("Running time for naive topRK Experiments (secs): ", (t2-t1)/1000);

        
        t1 = System.currentTimeMillis();
        runTopRKExperiments(sequenceGraph, resultDir);
        t2 = System.currentTimeMillis();
        runTimes.put("Running time for topRK Experiments (secs): ", (t2-t1)/1000);

        t1 = System.currentTimeMillis();
        runBootstrapExperiments(sequenceGraph, resultDir);
        t2 = System.currentTimeMillis();
        runTimes.put("Running time for bootstrap Experiments (secs): ", (t2-t1)/1000);
    }
    
    public static void runSequenceConnectExperiments(SequenceGraph sequenceGraph, String resultDir) {

    	Map<String, Double> runtimeMap = new HashMap<>();
    	
        for(double ciThreshold = 0.1; ciThreshold <= 0.25; ciThreshold+=0.05) {
            for(double piThreshold = 0.04; piThreshold <= 0.16; piThreshold+= 0.04) {
                SequenceGraph g = new SequenceGraph(sequenceGraph);
                String expID = "ci_0_" + (int)(ciThreshold * 1000) + "_pi_" + (int)(piThreshold * 1000);
                long t1 = System.currentTimeMillis(); // start measuring runtime here
                g.ciFilter(ciThreshold);
                Map<EventSequence, Double> results = new SequenceConnectMiner(g, piThreshold).getResultInMap();
                long t2 = System.currentTimeMillis(); // end of runtime measurement
                runtimeMap.put(expID, (t2-t1)/1000.0);
                SolgrindFileUtils.writeResultsInMap(results, Paths.get(resultDir + "sqconnect/result_ci_0_" + (int)(ciThreshold * 1000) + "_pi_" + (int)(piThreshold * 1000) + ".txt"));
            }
        }
        SolgrindFileUtils.writeResultsInMap(runtimeMap, Paths.get(resultDir + "sqconnect/individualRT.txt"));
    }

    public static void runEsGrowthExperiments(SequenceGraph sequenceGraph, String resultDir) {

        Map<String, Double> runtimeMap = new HashMap<>();

        for(double ciThreshold = 0.1; ciThreshold <= 0.25; ciThreshold+=0.05) {
            for(double piThreshold = 0.04; piThreshold <= 0.16; piThreshold+= 0.04) {
                SequenceGraph g = new SequenceGraph(sequenceGraph);
                String expID = "ci_0_" + (int)(ciThreshold * 1000) + "_pi_" + (int)(piThreshold * 1000);
                long t1 = System.currentTimeMillis(); // start measuring runtime here
                g.ciFilter(ciThreshold);
                Map<EventSequence, Double> results = new ESGrowthMiner(g, piThreshold).getResultInMap();
                long t2 = System.currentTimeMillis(); // end of runtime measurement
                runtimeMap.put(expID, (t2-t1)/1000.0);
                SolgrindFileUtils.writeResultsInMap(results, Paths.get(resultDir + "esgrowth/result_ci_0_" + (int)(ciThreshold * 1000) + "_pi_" + (int)(piThreshold * 1000) + ".txt"));
            }
        }
        SolgrindFileUtils.writeResultsInMap(runtimeMap, Paths.get(resultDir + "esgrowth/individualRT.txt"));
        
    }

    public static void runTopRKNaiveExperiments(SequenceGraph sequenceGraph, String resultDir) {

        Map<String, Double> runtimeMap = new HashMap<>();
        Map<String, Double> topKThresholdCI = new HashMap<>();

        for(double rPercent = 0.80; rPercent >= 0.20; rPercent-=0.20) {
            for(int k = 5; k <= 625; k=5*k) {
                SequenceGraph g = new SequenceGraph(sequenceGraph);
                String expID = "r_" + String.format ("%.2f", rPercent).replace(".", "_") + "_k_" + k;
                long t1 = System.currentTimeMillis(); // start measuring runtime here
                double threshold = g.findKPercentThreshold(rPercent);
                Map<EventSequence, Double> results = new NaiveTopRKESMiner(g, rPercent, k).getResultInMap();
                long t2 = System.currentTimeMillis(); // end of runtime measurement
                runtimeMap.put(expID, (t2-t1)/1000.0);
                topKThresholdCI.put(expID, threshold);
                SolgrindFileUtils.writeResultsInMap(results, Paths.get(resultDir + "naiveTopK/result_" + expID + ".txt"));
            }
        }
        SolgrindFileUtils.writeResultsInMap(runtimeMap, Paths.get(resultDir + "naiveTopK/individualRT.txt"));
        SolgrindFileUtils.writeResultsInMap(topKThresholdCI, Paths.get(resultDir + "naiveTopK/thresholds.txt"));
    }

    public static void runTopRKExperiments(SequenceGraph sequenceGraph, String resultDir) {

        Map<String, Double> runtimeMap = new HashMap<>();
        Map<String, Double> topKThresholdCI = new HashMap<>();

        for(double rPercent = 0.80; rPercent >= 0.20; rPercent-=0.2) {
            for(int k = 5; k <= 625; k=5*k) {
                SequenceGraph g = new SequenceGraph(sequenceGraph);
                String expID = "r_" + String.format ("%.2f", rPercent).replace(".", "_") + "_k_" + k;
                long t1 = System.currentTimeMillis(); // start measuring runtime here
                double threshold = g.findKPercentThreshold(rPercent);
                Map<EventSequence, Double> results = new TopRKESMiner(g, rPercent, k).getResultInMap();
                long t2 = System.currentTimeMillis(); // end of runtime measurement
                runtimeMap.put(expID, (t2-t1)/1000.0);
                topKThresholdCI.put(expID, threshold);
                SolgrindFileUtils.writeResultsInMap(results, Paths.get(resultDir + "topRK/result_" + expID + ".txt"));
            }
        }
        SolgrindFileUtils.writeResultsInMap(runtimeMap, Paths.get(resultDir + "topRK/individualRT.txt"));
        SolgrindFileUtils.writeResultsInMap(topKThresholdCI, Paths.get(resultDir + "topRK/thresholds.txt"));
    }

    public static void runBootstrapExperiments(SequenceGraph sequenceGraph, String resultDir) {

        Map<String, Double> runtimeMap = new HashMap<>();

        for(double reSampleRatio = 0.10; reSampleRatio <= 0.40; reSampleRatio+= 0.1) {
            for(int bootstrapCount = 50; bootstrapCount <= 200; bootstrapCount+= 50) {
                String expID = "rr_" + String.format ("%.2f", reSampleRatio).replace(".", "_") + "_btspk_" + bootstrapCount;
                long t1 = System.currentTimeMillis(); // start measuring runtime here
                Map<EventSequence, List<Double>> results =new BtspESMiner(new SequenceGraph(sequenceGraph), 0.5, 100).getResults();
                long t2 = System.currentTimeMillis(); // end of runtime measurement
                runtimeMap.put(expID, (t2-t1)/1000.0);
                SolgrindFileUtils.writeResultsInMap(results, Paths.get(resultDir + "bootstrap/result_" + expID + ".txt"));
            }
        }
        SolgrindFileUtils.writeResultsInMap(runtimeMap, Paths.get(resultDir + "bootstrap/individualRT.txt"));
    }

    private static ArrayList<EventType> createEventTypes(String[] eventTypeArray) {
        ArrayList<EventType> eventTypes = new ArrayList<>();
        for(String etStr : eventTypeArray){
            eventTypes.add(new EventType(etStr));
        }
        return eventTypes;
    }

}
