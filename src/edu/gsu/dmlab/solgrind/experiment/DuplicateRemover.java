package edu.gsu.dmlab.solgrind.experiment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.omg.Messaging.SyncScopeHelper;

import edu.gsu.dmlab.solgrind.algo.measures.implementation.OMIN;
import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;
import edu.gsu.dmlab.solgrind.fileio.CSVDataParser;
import edu.gsu.dmlab.solgrind.fileio.CSVDataWriter;

public class DuplicateRemover {

//	static final String[] eventTypeNames = {"ar", "ch", "ef", "fi", "fl", "sg", "ss"};
	static final String[] eventTypeNames = {"ef", "fl", "sg"};
	static Set<Instance> eventInstancesSet;
	static final double OMIN_THRESHOLD = 0.80;
	public static void driver(String[] args){

		CSVDataParser csvParser = new CSVDataParser();
		CSVDataWriter csvWriter = new CSVDataWriter();
		CSVDataParser.setDataDirectoryPath("/home/baydin2/Desktop/DuplicateRemoval/");
		CSVDataWriter.setOutputDir("/home/baydin2/Desktop/DuplicateRemoval/clean/");

		for(String etName : eventTypeNames){
//			if(!etName.equalsIgnoreCase("ef")){ //only get an event type
//				continue;
//			}
			System.out.println("EVENT TYPE: " + etName);
			eventInstancesSet = csvParser.readInstances(new EventType(etName));

			ArrayList<String> duplicates =  checkForDuplicates();
			ArrayList<Instance> list = new ArrayList<>(eventInstancesSet);
			for (Iterator<Instance> iter = list.listIterator(); iter.hasNext(); ) {
			    Instance ins = iter.next();
			    if (duplicates.contains(ins.getId())) {
			        iter.remove();
			    }
			}
			
			//System.out.println(eventInstancesSet.size());
			csvWriter.write(list, etName);
		}
		//System.out.println("Fixed Empty geometry count::: " + ComplexPolygonInterpolator.empty_count);
		//System.out.println("Areal invalid count::: " + ComplexPolygonInterpolator.arealInvalid_count);


	}
	private static ArrayList<String> checkForDuplicates() {
		ArrayList<Instance> instances = new ArrayList<>(eventInstancesSet);
		System.out.println("Number of instances: " + instances.size());
		HashSet<String> toBeRemoved = new HashSet<>();
		for(int i = 0; i < instances.size(); i++){
			//System.out.println("\t\t\t\t My first index is at " + i);
			Instance i1 = instances.get(i);
			if(toBeRemoved.contains(i1.getId())){
				continue;
			}

			for(int j = i+1; j < instances.size(); j++){
				
				
				Instance i2 = instances.get(j);
				if(toBeRemoved.contains(i2.getId())){
					continue;
				}

				double omin = OMIN.omin(i1, i2);


				if(omin > OMIN_THRESHOLD){
					double vol1 = i1.getTrajectory().getVolume();
					double vol2 = i2.getTrajectory().getVolume();
					if(vol1 > vol2){ //instance1 covers instance2 --> instance1 is the representative
						//add i2's id to to be removed
						toBeRemoved.add(i2.getId());
						System.out.println( "IDs: " + i1.getId() + " - " + i2.getId() + " OMIN: "+ omin );
						System.out.println( "\t\tPicking " + i1.getId() );
					} else{ //instance2 covers instance1 --> instance2 is the representative
						//add i1's id to to be removed
						System.out.println( "IDs: " + i1.getId() + " - " + i2.getId() + " OMIN: "+ omin );
						System.out.println( "\t\tPicking " + i2.getId() );
						toBeRemoved.add(i1.getId());

					}
				}
			}
		}
		System.out.println("");
		System.out.println(toBeRemoved.size());

		return new ArrayList<>(toBeRemoved);
	}
//	private static double getMaximumOMIN(HashSet<String> cluster, Instance i2) {
//		double maxOmin = 0.0;
//		for(String idCluster : cluster){
//			Instance insCluster = null;
//			for(Instance ins : eventInstancesSet){
//				if(ins.getId().equals(idCluster)){
//					insCluster = ins;
//					break;
//				}
//			}
//			if(insCluster == null){
//				System.err.println("FATAL ERROR. INSTANCE CANNOT BE NULL HERE");
//			}
//			double omin = OMIN.omin(insCluster, i2);
//			if( omin > maxOmin ){
//				maxOmin = omin;
//			}
//		}
//
//
//		return maxOmin;
//	}
//	private static int searchPossibleDuplicates(ArrayList<HashSet<String>> possibleDuplicates, String id) {
//		//		System.out.println("I am searching for " + id + " in " + possibleDuplicates);
//		for(int i = 0; i < possibleDuplicates.size(); i++){
//			HashSet<String> cluster = possibleDuplicates.get(i);
//			//			System.out.println("Cluster " + cluster);
//			for(String iid : cluster){
//				//				System.out.println("IID " + iid);
//				if(iid.equals(id)){
//					return i;
//				}
//			}
//		}
//
//		return -1;
//	}

}
