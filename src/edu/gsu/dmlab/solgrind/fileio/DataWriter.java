package edu.gsu.dmlab.solgrind.fileio;

import java.util.Collection;

import edu.gsu.dmlab.solgrind.base.Instance;

public abstract class DataWriter {


	public abstract void write(Collection<Instance> instances, String outfile);
	
}
