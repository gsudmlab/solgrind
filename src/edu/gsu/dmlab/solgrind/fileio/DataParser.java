package edu.gsu.dmlab.solgrind.fileio;

import java.util.Set;

import edu.gsu.dmlab.solgrind.base.EventType;
import edu.gsu.dmlab.solgrind.base.Instance;

public abstract class DataParser {
	
	
	
	public abstract Set<Instance> readInstances(EventType e);
	
	
	
}
	
	

